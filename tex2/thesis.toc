\contentsline {chapter}{Einleitung}{1}
\contentsline {chapter}{\numberline {1}Motivation und Zielsetzung}{3}
\contentsline {chapter}{\numberline {2}Grundlagen}{5}
\contentsline {section}{\numberline {2.1}Die Lithiumeisenphosphat - Sekund\IeC {\"a}rzelle}{5}
\contentsline {subsection}{\numberline {2.1.1}Prinzipieller Aufbau und Wirkungsweise}{5}
\contentsline {subsubsection}{Das Kathodenmaterial Lithiumeisenphosphat (LFP)}{6}
\contentsline {subsection}{\numberline {2.1.2}Spannungstypen und Kennlinie}{7}
\contentsline {subsection}{\numberline {2.1.3}Thermisches Verhalten}{8}
\contentsline {subsection}{\numberline {2.1.4}Weitere charakteristische Daten}{9}
\contentsline {section}{\numberline {2.2}Das Batteriespeichersystem}{11}
\contentsline {subsection}{\numberline {2.2.1}Aufbau und Verschaltung des Batteriepacks}{11}
\contentsline {subsubsection}{Serienschaltung}{11}
\contentsline {subsubsection}{Parallelschaltung}{12}
\contentsline {subsubsection}{Batterieverschaltung}{12}
\contentsline {subsection}{\numberline {2.2.2}Geh\IeC {\"a}use}{13}
\contentsline {subsection}{\numberline {2.2.3}Batterie - Management - System(BMS)}{14}
\contentsline {subsubsection}{Grundlagen}{14}
\contentsline {subsubsection}{Architekturen}{15}
\contentsline {section}{\numberline {2.3}W\IeC {\"a}rme\IeC {\"u}bertragung}{17}
\contentsline {subsection}{\numberline {2.3.1}W\IeC {\"a}rmeleitung}{17}
\contentsline {subsection}{\numberline {2.3.2}Konvektion}{18}
\contentsline {subsection}{\numberline {2.3.3}W\IeC {\"a}rmestrahlung}{19}
\contentsline {section}{\numberline {2.4}Normen und Standards}{21}
\contentsline {subsection}{\numberline {2.4.1} DIN EN 62619}{21}
\contentsline {subsection}{\numberline {2.4.2}VDE-AR-E 2510-50}{21}
\contentsline {subsection}{\numberline {2.4.3}UN 38.3}{21}
\contentsline {chapter}{\numberline {3}Experimentelle Methoden und Vorgehen}{23}
\contentsline {section}{\numberline {3.1}Messmethoden zur Temperaturmessung}{23}
\contentsline {subsection}{\numberline {3.1.1}Thermoelemente}{24}
\contentsline {subsection}{\numberline {3.1.2}Thermistoren}{24}
\contentsline {subsection}{\numberline {3.1.3}Ber\IeC {\"u}hrungslose Temperaturmessung}{25}
\contentsline {section}{\numberline {3.2}Temperatursensoren mit digitaler Schnittstelle}{27}
\contentsline {subsection}{\numberline {3.2.1}NTC}{27}
\contentsline {subsection}{\numberline {3.2.2}1-Wire Temperatursensor}{28}
\contentsline {subsection}{\numberline {3.2.3}I2C - Temperatursensor}{29}
\contentsline {section}{\numberline {3.3}Vorgehen und Versuchsger\IeC {\"a}te}{31}
\contentsline {subsection}{\numberline {3.3.1}Elektrische Betriebsmittel}{31}
\contentsline {subsection}{\numberline {3.3.2}Struktur der Messungen}{33}
\contentsline {chapter}{\numberline {4}Messtechnische Untersuchungen und Analyse der Ergebnisse}{35}
\contentsline {section}{\numberline {4.1}Temperaturprofil der LFP - Zelle}{35}
\contentsline {subsection}{\numberline {4.1.1}Thermisches Profil von Kathode, Anode und Zellk\IeC {\"o}rper}{35}
\contentsline {subsubsection}{Messaufbau}{36}
\contentsline {subsubsection}{Auswertung}{37}
\contentsline {section}{\numberline {4.2}Thermische Verteilung im Batteriepack}{38}
\contentsline {subsection}{\numberline {4.2.1}Leerstehendes Pack}{38}
\contentsline {subsubsection}{Messaufbau}{38}
\contentsline {subsubsection}{Auswertung}{39}
\contentsline {subsection}{\numberline {4.2.2}Verteilung im Pack mit Akkumulatoren}{40}
\contentsline {subsubsection}{ANSYS Simulation}{40}
\contentsline {subsubsection}{Messaufbau}{41}
\contentsline {subsubsection}{Auswertung}{42}
\contentsline {section}{\numberline {4.3}Versuche zur Sensorplatzierung}{44}
\contentsline {subsection}{\numberline {4.3.1}Vergleich: Sensoren an verschiedenen Stellen des Batteriepacks}{44}
\contentsline {subsubsection}{Messaufbau}{44}
\contentsline {subsubsection}{Auswertung}{45}
\contentsline {subsection}{\numberline {4.3.2}Einfluss der Kontaktplatten}{48}
\contentsline {subsubsection}{Messaufbau}{48}
\contentsline {subsubsection}{Auswertung}{49}
\contentsline {subsection}{\numberline {4.3.3}Vergleich: Sensoren in den \IeC {\"O}ffnungen des Innenraums}{50}
\contentsline {subsubsection}{Messaufbau}{52}
\contentsline {subsubsection}{Auswertung}{52}
\contentsline {subsection}{\numberline {4.3.4}Verifizierung der Platzierung}{55}
\contentsline {subsubsection}{Messaufbau}{55}
\contentsline {subsubsection}{Auswertung}{56}
\contentsline {chapter}{\numberline {5}Umsetzung des Messkonzepts}{58}
\contentsline {section}{\numberline {5.1}Finale Platzierung und Wahl der Sensoren}{58}
\contentsline {section}{\numberline {5.2}Implementierung des Messsystems}{59}
\contentsline {subsection}{\numberline {5.2.1}Spezifikationen f\IeC {\"u}r I2C - Kommunikation}{60}
\contentsline {subsection}{\numberline {5.2.2}Aufbau der Temperaturmessung}{61}
\contentsline {subsubsection}{Skizzierung der Verschaltung}{61}
\contentsline {subsubsection}{Flexible Printed Circuit Board (flexPCB)}{62}
\contentsline {section}{\numberline {5.3}Bewertung des Messsystems}{63}
\contentsline {chapter}{\numberline {6}Zusammenfassung und Ausblick}{66}
\contentsline {chapter}{\numberline {A}Zusammensetzung des Batteriepacks}{71}
