clc;

dat = readxls('ad.xls');
datenmenge = dat(1);
stats = datenmenge.value

t = stats(:,1)/3600;
y1 = stats(:,2);
y2 = stats(:,3);
y3 = stats(:,4);
y4 = stats(:,5);
y5 = stats(:,6);
y6 = stats(:,7);
y7 = stats(:,8);
y8 = stats(:,9);

//Plotten
scf(4);
clf();
xgrid(0);
plot2d(t,y1,6);//6
legend("$\text{NTC - Sensor 1}$")
plot2d(t,y2,7); //7
legend("$\text{NTC - Sensor 2}$")
plot2d(t,y3,16); //16
legend("$\text{NTC - Sensor 3}$")
plot2d(t,y4,5);//5
legend("$\text{NTC - Sensor 4}$")
plot2d(t,y5,2);//2
legend("$\text{NTC - Sensor 5}$")
plot2d(t,y6,3);//3
legend("$\text{NTC - Sensor 6}$")
plot2d(t,y7,4);//4
legend("$\text{NTC - Sensor 7}$")
plot2d(t,y8,1);//1
legend("$\text{NTC - Sensor 1}$","$\text{NTC - Sensor 2}$","$\text{NTC - Sensor 3}$", ..
"$\text{NTC - Sensor 4}$","$\text{NTC - Sensor 5}$","$\text{NTC - Sensor 6}$", ..
"$\text{NTC - Sensor 7}$","$\text{NTC - Sensor 8}$")

// Layout für pdf - Designs

f = gcf();
f.figure_size = [431,360];
f.axes_size = [425,340];
f.children.font_size = 0.5;
f.children.thickness = 0.5;
g = gce();
g.thickness = 0.5;
g.font_size = 0.5
zoom_rect([0,15,7,60]);

xlabel('$\text{Zeit in h}$', 'fontsize', 1);
ylabel('$\text{Temperatur in }^\circ\text{C}$', 'fontsize',1);

xs2pdf(4,"versuch1.pdf");

// Layout für PNG Designs
f = gcf();
f.figure_size = [1200,1000];
f.axes_size = [1150,950];
f.children.font_size = 5;
f.children.thickness = 1;

f.children.children(2).children.thickness = 3;
f.children.children(3).children.thickness = 3;
f.children.children(4).children.thickness = 3;
f.children.children(5).children.thickness = 3;
f.children.children(6).children.thickness = 3;
f.children.children(7).children.thickness = 3;
f.children.children(8).children.thickness = 3;
f.children.children(9).children.thickness = 3;

g = gce();
g.thickness = 3;
g.font_size = 5;

xlabel('$\text{Zeit in h}$', 'fontsize', 6);
ylabel('$\text{Temperatur in }^\circ\text{C}$', 'fontsize',6);
xs2png(4,"png_versuch.png")
