clc;

dat = readxls('tex_tmp_kat+anode(5).xls');
datenmenge = dat(1);
stats = datenmenge.value;

//Datenmenge für Kathoden/Anoden Zellwand Erwärmung
t1 = stats(:,1);
y1 = stats(:,2);
y2 = stats(:,3);
y3 = stats(:,4);

//Datenmenge für Erwärmung/Abkühlung
t2 = stats(:,6);
y4 = stats(:,7);


//Plotten
scf(3);
clf();
subplot(211); //Plotten der Kathoden-, Anoden- und Zellwand - Erwärmung
xgrid(0);
//Kathode
plot2d(t1,y1,6);
//Zellwand
plot2d(t1,y2,7);
//Anode
plot2d(t1,y3,16);
//zoom_rect([0,15,16,50]);
legend("$\text{Kathode}$","$\text{Zellwand}$","$\text{Anode}$",[2])
ylabel('$\text{Temperatur in }^\circ\text{C}$', 'fontsize',1);


subplot(212); //Plotten der Erwärmungs- und Abkühlkurve der Zelle
xgrid(0);
plot2d(t2,y4,5);

xlabel('$\text{Zeit in min}$', 'fontsize', 1);
ylabel('$\text{Temperatur in }^\circ\text{C}$', 'fontsize',1);

//zoom_rect([0,15,65,50]);

// Layout für pdf - Designs

f = gcf();
f.figure_size = [431,360];
f.axes_size = [425,340];
f.children.font_size = 0.5;
f.children.thickness = 0.5;





xs2pdf(3,"tex_tmp_kat+anode.pdf");
