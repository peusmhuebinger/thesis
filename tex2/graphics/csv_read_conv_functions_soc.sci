//Funktion für 

//Funktionen zum Einlesen und Verarbeiten von csv - Dateien
function [datFile]=read_csv_fct(fileName)
    sep = ";";
    ind = 1;
    menge_max = 500;
    irange = [ind 1 menge_max 1];
    //Start mit Einlesen von 1000 Reihenwerten pro Spalte
    //While-Schleife fragt ab, ob noch Reihenwerte zu lesen sind, 
    //anschließend werden die Reihenwerte dem maximalen Reihenwert
    //datFile zugeordnet
    test_read = csvRead(fileName,sep,",");
    [rows,columns] = size(test_read);
    datFile = [];
    while rows>0
        datString = csvRead(fileName,sep,",","string",[],[],irange); //liest csv von Spalte 1, 1000 Werte
        irange(1) = irange(1)+500;
        irange(3) = irange(3)+500;
        rows = rows - 500;
        datFile = [datFile;datString] 
    end  
endfunction



function [measurementTime, measurementValues]=read_grafana_csv(fileName)
    
    datFile = read_csv_fct(fileName);
    //Einlesen auch von den Werten der csv Datei
    datDouble = csvRead(fileName,";");
    [n1,n2] = size(datFile(:,1))
    [m1,m2] = size(datDouble(:,:))
    if n1 ~= m1 then
        printf("ERROR")
    end
    //Überspringen von Reihe 1, da nur Bezeichnungen der Reihen
    measurementTime = datDouble(2:n1,1)
    measurementValues = datDouble(2:m1,2:m2)
     
endfunction
