//NtTC und 1-Wire Sensoren Ausleser

clc;clear; 

//Funktionsaufruf
exec('csv_read_conv_functions.sci')

//Korrektes Einlesen des csv-files
fileName1 = "csv_files/ntc_export.csv"
fileName2 = "csv_files/1wire_export.csv"

[timeValues1, measurementValue1]=read_grafana_csv(fileName1);
[timeValues2, measurementValue2]=read_grafana_csv(fileName2);

//Normierung Zeitwert in Sekunde(1)/Minute(60)/Stunde(3600)
t1 = timeValues1/3600; //NTCs
t2 = timeValues2/3600; //1Wire

//Kalibrierwert
calibration = measurementValue1(:,8);
//Einlesen von 8 y-Werten NTC
y11 = measurementValue1(:,1) - calibration; 
y12 = measurementValue1(:,2) - calibration;
y13 = measurementValue1(:,3) - calibration;
y14 = measurementValue1(:,4) - calibration;
y15 = measurementValue1(:,5) - calibration;
y16 = measurementValue1(:,6) - calibration;
y17 = measurementValue1(:,7) - calibration;


y21 = measurementValue2(:,1) - calibration;
y22 = measurementValue2(:,2) - calibration;
y23 = measurementValue2(:,3) - calibration;
y24 = measurementValue2(:,4) - calibration;
y25 = measurementValue2(:,5) - calibration;
y26 = measurementValue2(:,6) - calibration;
y27 = measurementValue2(:,7) - calibration;

///////////////////////////////////////////GRAPH 1 /////////////////////////////////////////////////////

//Graphik - Einstellungen
//figure(4);
scf(4); //Nummer des Graphik-Fensters
clf(); //Löschen des Graphikhintergrunds
zoom_rect = ([0,0,12,30]);
xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(t1,y15,color("grey"),rect=[0,0,12,30]);//2
plot2d(t2,y26,5,rect=[0,0,12,30]);// z,a 1-Wire
plot2d(t1,y11,16,rect=[0,0,12,30]);//z,i NTC
plot2d(t1,y12,1,rect=[0,0,12,30]); //z,a NTC
//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Erwärmung Heizelement}$","$\text{1-Wire z,a}$","$\text{NTC z,i}$","$\text{NTC z,a}$")


// Layout für PNG Designs
f = gcf();
f.figure_size = [1500,1000]; //For PNG Desine
f.axes_size = [1350,950]; //Graph size
f.children.font_size = 5; //Achsendicke
f.children.thickness = 1; //Grid Dicke

f.children.children(2).children.thickness = 3; //Polyline features (Dicke der Linien)
f.children.children(3).children.thickness = 3;
f.children.children(4).children.thickness = 3;
f.children.children(5).children.thickness = 3;
f.children.children(5).children.line_style = 8;
//f.children.children(7).children.thickness = 3;
//f.children.children(8).children.thickness = 3;

g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 4;  //Schriftgröße

//Labels '$' in Latex-Schreibweise
xlabel('$\text{Zeit in h}$', 'fontsize', 6);
ylabel('$\Delta\text{T in}^\circ\text{C}$', 'fontsize',6);
title('$\text{Vergleich 1-Wire und NTC 0,5er Zellenabstand}$','fontsize',6);

xs2png(4,"graphic_output/ntc_1wire_zentral.png");

///////////////////////////////////////////GRAPH 2 /////////////////////////////////////////////////////

//Graphik - Einstellungen
//figure(5);
scf(5); //Nummer des Graphik-Fensters
clf(); //Löschen des Graphikhintergrunds
zoom_rect = ([0,0,12,30]);
xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(t1,y15,color("grey"),rect=[0,0,12,30]);//2
plot2d(t2,y25,5,rect=[0,0,12,30]);// o,a 1-Wire
plot2d(t1,y13,1,rect=[0,0,12,30]);//o,a NTC
//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Erwärmung Heizelement}$","$\text{1-Wire o,a}$","$\text{NTC o,a}$")


// Layout für PNG Designs
f = gcf();
f.figure_size = [1500,1000]; //For PNG Desine
f.axes_size = [1350,950]; //Graph size
f.children.font_size = 5; //Achsendicke
f.children.thickness = 1; //Grid Dicke

f.children.children(2).children.thickness = 3; //Polyline features (Dicke der Linien)
f.children.children(3).children.thickness = 3;
f.children.children(4).children.thickness = 3;
f.children.children(4).children.line_style = 8;
//f.children.children(7).children.thickness = 3;
//f.children.children(8).children.thickness = 3;

g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 4;  //Schriftgröße

//Labels '$' in Latex-Schreibweise
xlabel('$\text{Zeit in h}$', 'fontsize', 6);
ylabel('$\Delta\text{T in}^\circ\text{C}$', 'fontsize',6);
title('$\text{Vergleich 1-Wire und NTC 1 Zellenabstand nach oben}$','fontsize',6);

xs2png(5,"graphic_output/ntc_1wire_oben.png");

///////////////////////////////////////////GRAPH 3 /////////////////////////////////////////////////////

//Graphik - Einstellungen
//figure(6);
scf(6); //Nummer des Graphik-Fensters
clf(); //Löschen des Graphikhintergrunds
zoom_rect = ([0,0,12,30]);
xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(t1,y15,color("grey"),rect=[0,0,12,30]);//2
plot2d(t2,y27,5,rect=[0,0,12,30]);// u,a 1-Wire
plot2d(t1,y14,1,rect=[0,0,12,30]);//u,a NTC
//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Erwärmung Heizelement}$","$\text{1-Wire u,a}$","$\text{NTC u,a}$")


// Layout für PNG Designs
f = gcf();
f.figure_size = [1500,1000]; //For PNG Desine
f.axes_size = [1350,950]; //Graph size
f.children.font_size = 5; //Achsendicke
f.children.thickness = 1; //Grid Dicke

f.children.children(2).children.thickness = 3; //Polyline features (Dicke der Linien)
f.children.children(3).children.thickness = 3;
f.children.children(4).children.thickness = 3;
f.children.children(4).children.line_style = 8;
//f.children.children(7).children.thickness = 3;
//f.children.children(8).children.thickness = 3;

g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 4;  //Schriftgröße

//Labels '$' in Latex-Schreibweise
xlabel('$\text{Zeit in h}$', 'fontsize', 6);
ylabel('$\Delta\text{T in}^\circ\text{C}$', 'fontsize',6);
title('$\text{Vergleich 1-Wire und NTC 1 Zellenabstand unten}$','fontsize',6);

xs2png(6,"graphic_output/ntc_1wire_unten.png");

///////////////////////////////////////////GRAPH 4 /////////////////////////////////////////////////////

//Graphik - Einstellungen
//figure(7);
scf(7); //Nummer des Graphik-Fensters
clf(); //Löschen des Graphikhintergrunds
zoom_rect = ([0,0,12,30]);
xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(t1,y15,color("grey"),rect=[0,0,12,30]);//2
plot2d(t2,y25,5,rect=[0,0,12,30]);// l,a 1-Wire
plot2d(t1,y17,1,rect=[0,0,12,30]);//l,a NTC
//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Erwärmung Heizelement}$","$\text{1-Wire l,a}$","$\text{NTC l,a}$")


// Layout für PNG Designs
f = gcf();
f.figure_size = [1500,1000]; //For PNG Desine
f.axes_size = [1350,950]; //Graph size
f.children.font_size = 5; //Achsendicke
f.children.thickness = 1; //Grid Dicke

f.children.children(2).children.thickness = 3; //Polyline features (Dicke der Linien)
f.children.children(3).children.thickness = 3;
f.children.children(4).children.thickness = 3;
f.children.children(4).children.line_style = 8;
//f.children.children(7).children.thickness = 3;
//f.children.children(8).children.thickness = 3;

g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 4;  //Schriftgröße

//Labels '$' in Latex-Schreibweise
xlabel('$\text{Zeit in h}$', 'fontsize', 6);
ylabel('$\Delta\text{T in}^\circ\text{C}$', 'fontsize',6);
title('$\text{Vergleich 1-Wire und NTC 1 Zellenabstand nach links}$','fontsize',6);

xs2png(7,"graphic_output/ntc_1wire_links.png");
