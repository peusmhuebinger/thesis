//Verarbeitungsprogramm und Plotprogramm für 8 Sensoren
clc;clear; 

//Funktionsaufruf
exec('csv_read_conv_functions.sci')

//Korrektes Einlesen des csv-files
fileName = "csv_files/test_file.csv"
[timeValues, measurementValue]=read_grafana_csv(fileName);

//Normierung Zeitwert in Sekunde(1)/Minute(60)/Stunde(3600)
t = timeValues/3600;
//Einlesen von 8 y-Werten
y1 = measurementValue(:,1);
y2 = measurementValue(:,2);
y3 = measurementValue(:,3);
y4 = measurementValue(:,4);
y5 = measurementValue(:,5);
y6 = measurementValue(:,6);
y7 = measurementValue(:,7);
y8 = measurementValue(:,8);


//Graphik - Einstellungen

scf(4); //Nummer des Graphik-Fensters
clf(); //Löschen des Graphikhintergrunds
xgrid(0); //Schwarzes Grid

//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(t,y1,7);//6
plot2d(t,y2,3); //7
plot2d(t,y3,color("grey")); //16
plot2d(t,y4,color("grey"));//5
plot2d(t,y5,color("grey"));//2
plot2d(t,y6,5);//3
plot2d(t,y7,4);//4
plot2d(t,y8,color("grey"));//1
//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Sensor A(-0,5/-0,5)}$","$\text{Sensor A(0/-1)}$","$\text{Sensor A(-1/0)}$", ..
"$\text{Sensor A(0/0)}$","$\text{Sensor A(1/0)}$","$\text{Sensor A(0/1)}$", ..
"$\text{Sensor A(0,5/0,5)}$","$\text{Sensor A(3/0)}$")


// Layout für PNG Designs
f = gcf();
f.figure_size = [1200,1000]; //For PNG Desine
f.axes_size = [1150,950]; //Graph size
f.children.font_size = 5; //Achsendicke
f.children.thickness = 1; //Grid Dicke

f.children.children(2).children.thickness = 3; //Polyline features (Dicke der Linien)
f.children.children(3).children.thickness = 3;
f.children.children(4).children.thickness = 3;
f.children.children(5).children.thickness = 3;
f.children.children(6).children.thickness = 3;
f.children.children(7).children.thickness = 3;
f.children.children(8).children.thickness = 3;
f.children.children(9).children.thickness = 3;

g = gce(); //Legenden Menü
g.thickness = 3;  //Dicke
g.font_size = 5;  //Schriftgröße

//Labels '$' in Latex-Schreibweise
xlabel('$\text{Zeit in h}$', 'fontsize', 6);
ylabel('$\text{Temperatur in }^\circ\text{C}$', 'fontsize',6);

//PNG Graphik Export
xs2png(4,"graphic_output/test_file.png")
