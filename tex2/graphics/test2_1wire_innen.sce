//Test1

clc;clear; 

//Funktionsaufruf
exec('csv_read_conv_functions.sci')

//Korrektes Einlesen des csv-files
fileName1 = "csv_files/grafana_data_export_test3.csv"

[timeValues1, measurementValue1]=read_grafana_csv(fileName1);


//Normierung Zeitwert in Sekunde(1)/Minute(60)/Stunde(3600)
t1 = timeValues1/3600; //1Wire außen

//Kalibrierwert
calibration = measurementValue1(:,17);
//Einlesen von Außen Sensoren
y101 = measurementValue1(:,1) - calibration; 
y102 = measurementValue1(:,2) - calibration;
y103 = measurementValue1(:,3) - calibration;
y104 = measurementValue1(:,4) - calibration;
y105 = measurementValue1(:,5) - calibration;
y106 = measurementValue1(:,6) - calibration;
y107 = measurementValue1(:,7) - calibration;
y108 = measurementValue1(:,8) - calibration; 
y109 = measurementValue1(:,9) - calibration;
y110 = measurementValue1(:,10) - calibration;
y111 = measurementValue1(:,11) - calibration;
y112 = measurementValue1(:,12) - calibration;
y113 = measurementValue1(:,13) - calibration;
y114 = measurementValue1(:,14) - calibration;
y115 = measurementValue1(:,15) - calibration;
y116 = measurementValue1(:,16) - calibration;


///////////////////////////////////////////GRAPH 1 /////////////////////////////////////////////////////

//Graphik - Einstellungen
//figure(4);
scf(0); //Nummer des Graphik-Fensters
clf(); //Löschen des Graphikhintergrunds
xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot(t1,y103);//5
plot(t1,y111);//7
plot(t1,y112);//8
//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Sensor 3}$","$\text{Sensor 11}$","$\text{Sensor 12}$",[2])
title('$\text{Interne Wärmeverteilung auf Sockel (Sensor3) und in Kammern (Sensor11,12)}$','fontsize',6);

// Layout für PNG Designs
f = gcf();
f.figure_size = [1500,1000]; //For PNG Desine
f.axes_size = [1350,950]; //Graph size
f.children.font_size = 5; //Achsendicke
f.children.thickness = 1; //Grid Dicke

f.children.children(2).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(2).children.foreground = 5;
f.children.children(2).children.mark_style = 5;
f.children.children(2).children.mark_foreground = 5;
f.children.children(3).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(3).children.foreground = 3;
f.children.children(3).children.mark_style = 3;
f.children.children(3).children.mark_foreground = 3;
f.children.children(4).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(4).children.foreground = 4; //Polyline features (Dicke der Linien)
f.children.children(4).children.mark_style = 4;
f.children.children(4).children.mark_foreground = 4;
//f.children.children(7).children.thickness = 3;
//f.children.children(8).children.thickness = 3;

g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 4;  //Schriftgröße

//Labels '$' in Latex-Schreibweise
xlabel('$\text{Zeit in h}$', 'fontsize', 6);
ylabel('$\Delta\text{T in}^\circ\text{C}$', 'fontsize',6);


xs2png(0,"graphic_output/test3_31112.png");
