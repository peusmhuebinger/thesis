//NtTC und 1-Wire Sensoren Ausleser

clc;clear; 

//Funktionsaufruf
exec('csv_read_conv_functions.sci')

//Korrektes Einlesen des csv-files
fileName1 = "csv_files/grafana_data_export_1wire_außen.csv"
fileName2 = "csv_files/grafana_data_export_1wire_innen.csv"

[timeValues1, measurementValue1]=read_grafana_csv(fileName1);
[timeValues2, measurementValue2]=read_grafana_csv(fileName2);

//Normierung Zeitwert in Sekunde(1)/Minute(60)/Stunde(3600)
t1 = timeValues1/3600; //1Wire außen
t2 = timeValues2/3600; //1Wire innen

//Kalibrierwert
calibration = measurementValue1(:,17);
//Einlesen von Außen Sensoren
y101 = measurementValue1(:,1) - calibration; 
y102 = measurementValue1(:,2) - calibration;
y103 = measurementValue1(:,3) - calibration;
y104 = measurementValue1(:,4) - calibration;
y105 = measurementValue1(:,5) - calibration;
y106 = measurementValue1(:,6) - calibration;
y107 = measurementValue1(:,7) - calibration;
y108 = measurementValue1(:,8) - calibration; 
y109 = measurementValue1(:,9) - calibration;
y110 = measurementValue1(:,10) - calibration;
y111 = measurementValue1(:,11) - calibration;
y112 = measurementValue1(:,12) - calibration;
y113 = measurementValue1(:,13) - calibration;
y114 = measurementValue1(:,14) - calibration;
y115 = measurementValue1(:,15) - calibration;
y116 = measurementValue1(:,16) - calibration;

//Einlesen von Innen Sensoren
y201 = measurementValue2(:,1) - calibration; 
y202 = measurementValue2(:,2) - calibration;
y203 = measurementValue2(:,3) - calibration;
y204 = measurementValue2(:,4) - calibration;
y205 = measurementValue2(:,5) - calibration;
y206 = measurementValue2(:,6) - calibration;
y207 = measurementValue2(:,7) - calibration;
y208 = measurementValue2(:,8) - calibration; 
y209 = measurementValue2(:,9) - calibration;
y210 = measurementValue2(:,10) - calibration;
y211 = measurementValue2(:,11) - calibration;
y212 = measurementValue2(:,12) - calibration;
y213 = measurementValue2(:,13) - calibration;
y214 = measurementValue2(:,14) - calibration;
y215 = measurementValue2(:,15) - calibration;
y216 = measurementValue2(:,16) - calibration;

///////////////////////////////////////////GRAPH 1 /////////////////////////////////////////////////////

//Graphik - Einstellungen
//figure(4);
scf(0); //Nummer des Graphik-Fensters
figure(0)
subplot(211)
//clf(); //Löschen des Graphikhintergrunds
xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(t1,y104,1,rect=[0,-5,5,5]);//2
plot2d(t2,y204,5,rect=[0,-5,5,5]);// z,a 1-Wire
//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Sensor 4 außen}$","$\text{Sensor  innen}$")
title('$\text{Vergleich Sensor am Heizelement innen und außen Pack}$','fontsize',6);

// Layout für PNG Designs
f = gcf();
f.figure_size = [1500,1000]; //For PNG Desine
f.axes_size = [1350,950]; //Graph size
f.children.font_size = 5; //Achsendicke
f.children.thickness = 1; //Grid Dicke

f.children.children(2).children.thickness = 3; //Polyline features (Dicke der Linien)
f.children.children(3).children.thickness = 3;
//f.children.children(7).children.thickness = 3;
//f.children.children(8).children.thickness = 3;

g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 4;  //Schriftgröße

//Labels '$' in Latex-Schreibweise

ylabel('$\Delta\text{T in}^\circ\text{C}$', 'fontsize',6);


subplot(212)
//clf(); //Löschen des Graphikhintergrunds
xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(t1,y116,1,rect=[0,-5,5,5]);//2
plot2d(t2,y216,5,rect=[0,-5,5,5]);// z,a 1-Wire
//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Sensor 16 außen}$","$\text{Sensor 16 innen}$")


// Layout für PNG Designs
f = gcf();
f.figure_size = [1500,1000]; //For PNG Desine
f.axes_size = [1350,950]; //Graph size
f.children.font_size = 5; //Achsendicke
f.children.thickness = 1; //Grid Dicke

f.children.children(2).children.thickness = 3; //Polyline features (Dicke der Linien)
f.children.children(3).children.thickness = 3;
//f.children.children(7).children.thickness = 3;
//f.children.children(8).children.thickness = 3;

g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 4;  //Schriftgröße

//Labels '$' in Latex-Schreibweise
xlabel('$\text{Zeit in h}$', 'fontsize', 6);
ylabel('$\Delta\text{T in}^\circ\text{C}$', 'fontsize',6);


xs2png(0,"graphic_output/1wire_vergleich_min_test1.png");





