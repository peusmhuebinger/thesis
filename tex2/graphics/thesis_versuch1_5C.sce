
//Thesis Versuch 1 -> Temperatur an Kathode und Anode

clc;clear; 

//Funktionsaufruf
exec('csv_read_conv_functions.sci')

//Korrektes Einlesen des csv-files
fileName1 = "csv_files/Neu1_5C.csv"

[timeValues1, measurementValue1]=read_grafana_csv(fileName1);


//Normierung Zeitwert in Sekunde(1)/Minute(60)/Stunde(3600)
t1 = timeValues1/60; //1Wire außen

//Kalibrierwert
calibration = measurementValue1(:,4);
//Einlesen von Außen Sensoren
y101 = measurementValue1(:,1) - calibration; 
y102 = measurementValue1(:,2) - calibration;
y103 = measurementValue1(:,3) - calibration;


//Graphik - Einstellungen
//figure(4);
scf(1); //Nummer des Graphik-Fensters
clf(); //Löschen des Graphikhintergrunds
xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(t1,y101,5);
plot2d(t1,y102,4);
plot2d(t1,y103,3);

//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Kathode}$","$\text{Oberfläche}$","$\text{Anode}$",[2])

// Layout für TEX Designs
f = gcf();
f.figure_size = [431,360];
f.axes_size = [425,340];
f.children.font_size = 0.5;
f.children.thickness = 0.5;

f.children.children(2).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(2).children.foreground = 5;
f.children.children(2).children.mark_style = 5;
f.children.children(2).children.mark_foreground = 5;
f.children.children(3).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(3).children.foreground = 3;
f.children.children(3).children.mark_style = 3;
f.children.children(3).children.mark_foreground = 3;
f.children.children(4).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(4).children.foreground = 13; //Polyline features (Dicke der Linien)
f.children.children(4).children.mark_style = 13;
f.children.children(4).children.mark_foreground = 13;

//f.children.children(7).children.thickness = 3;
//f.children.children(8).children.thickness = 3;

g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 1;  //Schriftgröße

//Labels '$' in Latex-Schreibweise
xlabel('$\text{Zeit/min}$', 'fontsize', 2);
ylabel('$\Delta\text{T/K}$', 'fontsize',2);


xs2pdf(1,"graphic_output/thesis_versuch1_5C.pdf");



