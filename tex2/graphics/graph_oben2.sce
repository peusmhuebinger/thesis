//Präsentation Anfang November - Graph graue Verteilung

clc;

dat = readxls('ad.xls');
datenmenge = dat(1);
stats = datenmenge.value

t = stats(:,1)/3600;
y1 = stats(:,2);
y2 = stats(:,3);
y3 = stats(:,4);
y4 = stats(:,5);
y5 = stats(:,6);
y6 = stats(:,7);
y7 = stats(:,8);
y8 = stats(:,9);

//Plotten
scf(4);
clf();
xgrid(0);
plot2d(t,y1,7);//6  A(-0,5/-0,5)
legend("$\text{NTC - Sensor 1}$")
plot2d(t,y2,3); //7  A(0/-1) 
legend("$\text{NTC - Sensor 2}$")
plot2d(t,y3,color("grey")); //16  A(-1/0)
legend("$\text{NTC - Sensor 3}$")
plot2d(t,y4,color("grey"));//5  A(0/0)
legend("$\text{NTC - Sensor 4}$")
plot2d(t,y5,color("grey"));//2  A(1/0)
legend("$\text{NTC - Sensor 5}$")
plot2d(t,y6,5);//3  A(0/1)
legend("$\text{NTC - Sensor 6}$")
plot2d(t,y7,4);//4  A(0,5/0,5)
legend("$\text{NTC - Sensor 7}$")
plot2d(t,y8,color("grey"));//1  A(3/0)
legend("$\text{Sensor A(-0,5/-0,5)}$","$\text{Sensor A(0/-1)}$","$\text{Sensor A(-1/0)}$", ..
"$\text{Sensor A(0/0)}$","$\text{Sensor A(1/0)}$","$\text{Sensor A(0/1)}$", ..
"$\text{Sensor A(0,5/0,5)}$","$\text{Sensor A(3/0)}$")


// Layout für PNG Designs
f = gcf();
f.figure_size = [1200,1000];
f.axes_size = [1150,950];
f.children.font_size = 5;
f.children.thickness = 1;

f.children.children(2).children.thickness = 3;
f.children.children(3).children.thickness = 3;
f.children.children(4).children.thickness = 3;
f.children.children(5).children.thickness = 3;
f.children.children(6).children.thickness = 3;
f.children.children(7).children.thickness = 3;
f.children.children(8).children.thickness = 3;
f.children.children(9).children.thickness = 3;

g = gce();
g.thickness = 3;
g.font_size = 5;

xlabel('$\text{Zeit in h}$', 'fontsize', 6);
ylabel('$\text{Temperatur in }^\circ\text{C}$', 'fontsize',6);
xs2png(4,"graph_oben2.png")
