//NtTC und 1-Wire Sensoren Ausleser

clc;clear; 

//Funktionsaufruf
exec('csv_read_conv_functions.sci')

//Korrektes Einlesen des csv-files
fileName1 = "csv_files/ntc_export.csv"
fileName2 = "csv_files/1wire_export.csv"

[timeValues1, measurementValue1]=read_grafana_csv(fileName1);
[timeValues2, measurementValue2]=read_grafana_csv(fileName2);

//Normierung Zeitwert in Sekunde(1)/Minute(60)/Stunde(3600)
t1 = timeValues1/3600; //NTCs
t2 = timeValues2/3600; //1Wire

//Kalibrierwert
calibration = measurementValue1(:,8);
//Einlesen von 8 y-Werten NTC
y11 = measurementValue1(:,1) - calibration; 
y12 = measurementValue1(:,2) - calibration;
y13 = measurementValue1(:,3) - calibration;
y14 = measurementValue1(:,4) - calibration;
y15 = measurementValue1(:,5) - calibration;
y16 = measurementValue1(:,6) - calibration;
y17 = measurementValue1(:,7) - calibration;


y21 = measurementValue2(:,1) - calibration;
y22 = measurementValue2(:,2) - calibration;
y23 = measurementValue2(:,3) - calibration;
y24 = measurementValue2(:,4) - calibration;
y25 = measurementValue2(:,5) - calibration;
y26 = measurementValue2(:,6) - calibration;
y27 = measurementValue2(:,7) - calibration;



//Graphik - Einstellungen

scf(4); //Nummer des Graphik-Fensters
clf(); //Löschen des Graphikhintergrunds

subplot(211);
zoom_rect = ([0,0,12,30]);
xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(t1,y15,color("grey"),rect=[0,0,12,30]);//2
plot2d(t1,y11,1,rect=[0,0,12,30]);//z,i
plot2d(t1,y12,3,rect=[0,0,12,30]); //z,a
plot2d(t1,y13,4,rect=[0,0,12,30]); //o,a
plot2d(t1,y14,5,rect=[0,0,12,30]);//u,a
//plot2d(t1,y16,5,rect=[0,0,12,30]);//3
plot2d(t1,y17,16,rect=[0,0,12,30]);//l,a
//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Erwärmung Heizelement}$","$\text{Sensor z,i}$","$\text{Sensor z,a}$","$\text{Sensor o,a}$", ..
"$\text{Sensor u,a}$","$\text{Sensor l,a}$")


// Layout für PNG Designs
f = gcf();
f.figure_size = [1500,1000]; //For PNG Desine
f.axes_size = [1350,950]; //Graph size
f.children.font_size = 5; //Achsendicke
f.children.thickness = 1; //Grid Dicke

f.children.children(2).children.thickness = 3; //Polyline features (Dicke der Linien)
f.children.children(3).children.thickness = 3;
f.children.children(4).children.thickness = 3;
f.children.children(5).children.thickness = 3;
f.children.children(6).children.thickness = 3;
f.children.children(7).children.thickness = 3;
f.children.children(7).children.line_style = 8;
//f.children.children(7).children.thickness = 3;
//f.children.children(8).children.thickness = 3;

g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 4;  //Schriftgröße

//Labels '$' in Latex-Schreibweise
ylabel('$\Delta\text{T in}^\circ\text{C}$', 'fontsize',6);
title('$\text{NTC - Sensoren}$','fontsize',6);

subplot(212);
xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
//plot2d(t2,y21,7,rect=[0,0,12,30]);//6
plot2d(t1,y15,color("grey"),rect=[0,0,12,30]);//2
plot2d(t2,y22,4,rect=[0,0,12,30]); //l,a
//plot2d(t2,y23,color("grey"),rect=[0,0,12,30]); //16
//plot2d(t2,y24,color("grey"),rect=[0,0,12,30]);//5
plot2d(t2,y25,16,rect=[0,0,12,30]);//o,a
plot2d(t2,y26,3,rect=[0,0,12,30]);// z,a
plot2d(t2,y27,5,rect=[0,0,12,30]);//u,a
//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Erwärmung Heizelement}$","$\text{Sensor o,a}$","$\text{Sensor l,a}$","$\text{Sensor z,a}$", ..
"$\text{Sensor u,a}$")

// Layout für PNG Designs
f = gcf();
f.figure_size = [1500,1000]; //For PNG Desine
f.axes_size = [1350,950]; //Graph size
f.children.font_size = 5; //Achsendicke
f.children.thickness = 1; //Grid Dicke

f.children.children(2).children.thickness = 3; //Polyline features (Dicke der Linien)
f.children.children(3).children.thickness = 3;
f.children.children(4).children.thickness = 3;
f.children.children(5).children.thickness = 3;
f.children.children(6).children.thickness = 3;
f.children.children(6).children.line_style = 8;
//f.children.children(6).children.thickness = 3;
//f.children.children(7).children.thickness = 3;
//f.children.children(8).children.thickness = 3;

g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 4;  //Schriftgröße

//Labels '$' in Latex-Schreibweise
xlabel('$\text{Zeit in h}$', 'fontsize', 6);
ylabel('$\Delta\text{T in}^\circ\text{C}$', 'fontsize',6);
title('$\text{1-Wire - Sensoren}$','fontsize',6);

//PNG Graphik Export
xs2png(4,"graphic_output/ntc_1wire.png")
