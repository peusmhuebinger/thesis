

//Funktionen zum Einlesen und Verarbeiten von csv - Dateien
function [datFile]=read_csv_fct(fileName)
    sep = ";";
    ind = 1;
    menge_max = 500;
    irange = [ind 1 menge_max 1];
    //Start mit Einlesen von 1000 Reihenwerten pro Spalte
    //While-Schleife fragt ab, ob noch Reihenwerte zu lesen sind, 
    //anschließend werden die Reihenwerte dem maximalen Reihenwert
    //datFile zugeordnet
    test_read = csvRead(fileName,sep,",");
    [rows,columns] = size(test_read);
    datFile = [];
    while rows>0
        datString = csvRead(fileName,sep,",","string",[],[],irange); //liest csv von Spalte 1, 1000 Werte
        irange(1) = irange(1)+500;
        irange(3) = irange(3)+500;
        rows = rows - 500;
        datFile = [datFile;datString] 
    end  
endfunction



function [timeValues, measurementValues]=read_grafana_csv(fileName)
    
    datFile = read_csv_fct(fileName);
    //Einlesen auch von den Werten der csv Datei
    datDouble = csvRead(fileName,";");
    [n1,n2] = size(datFile(:,1))
    [m1,m2] = size(datDouble(:,:))
    if n1 ~= m1 then
        printf("ERROR")
    end
    //Überspringen von Reihe 1, da nur Bezeichnungen der Reihen
    measurementTime = datFile(2:n1,1)
    measurementValues = datDouble(2:m1,2:m2)
    
    //Umrechnung des String mit Sonderzeichen in konvertierte Integer Array-Schreibweise ohne Sonderzeichen
    timeArrayConverted = msscanf( measurementTime(1),'%d%c%d%c%d%c%d%c%d%c%d') //Example: 2018-12-25-09-45-30/ Datum/Zeit: 25.12.2018 ->9:45:30 Uhr 
    t0(1:6) = timeArrayConverted(1:2:11) //zeitlicher Startwert
    
    //For-Schleife regelt mit Inkrementierung Berechnung aller Spalten-Werte der Zeitanzeige; etime-function regelt Startwert
    for i = 1:n1-1
        timeArrayConverted = msscanf( measurementTime(i),'%d%c%d%c%d%c%d%c%d%c%d')
        timeArrayInt(1:6) = timeArrayConverted(1:2:11)
        timeValues(i)=etime(timeArrayInt,t0);
    end 
endfunction
