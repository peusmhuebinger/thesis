// thesis platten vergleich

clc;clear; 

//Funktionsaufruf
exec('csv_read_conv_functions.sci')

//Korrektes Einlesen des csv-files
fileName1 = "csv_files/platten_export.csv"

[timeValues1, measurementValue1]=read_grafana_csv(fileName1);


//Normierung Zeitwert in Sekunde(1)/Minute(60)/Stunde(3600)
t1 = timeValues1/60; //1Wire außen

calibration = measurementValue1(:,7) //Sensor_Umgebung
//Einlesen von Außen Sensoren
y101 = measurementValue1(:,1) - calibration //Sensor5
y102 = measurementValue1(:,2) //Sensor6
y103 = measurementValue1(:,3) - calibration //Sensor7
y104 = measurementValue1(:,4) - calibration //Sensor9
y105 = measurementValue1(:,5) //Sensor10
y106 = measurementValue1(:,6) - calibration //Sensor11


//Graphik - Einstellungen
//figure(4);
subplot(211)


xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(t1,y106);
plot2d(t1,y101);
//plot2d(t1,delta_y101,8);
//plot2d(t1,delta_y102,8);
//plot2d(t1,delta_y103,8);
//plot2d(t1,delta_y104,8);
//plot2d(t1, y105);
//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Sensor1}$","$\text{Sensor6}$",[4])

// Layout für TEX Designs
f = gcf();
f.figure_size = [431,360];
f.axes_size = [425,340];
f.children.font_size = 0.5;
f.children.thickness = 0.5;

f.children.children(2).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(2).children.foreground = 5;
f.children.children(2).children.mark_style = 0;
f.children.children(2).children.mark_foreground = 5;
f.children.children(3).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(3).children.foreground = 13;
f.children.children(3).children.mark_style = 0;
f.children.children(3).children.mark_foreground = 13;


//f.children.children(7).children.thickness = 3;
//f.children.children(8).children.thickness = 3;

g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 1;  //Schriftgröße

//Labels '$' in Latex-Schreibweise
ylabel('$\Delta\text{T/K}$', 'fontsize',2);


subplot(212);

xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(t1, y104);
plot2d(t1, y103);


//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Sensor3}$","$\text{Sensor4}$",[4])

// Layout für TEX Designs
f = gcf();
f.figure_size = [431,360];
f.axes_size = [425,340];
f.children.font_size = 0.5;
f.children.thickness = 0.5;

f.children.children(2).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(2).children.foreground = 5;
f.children.children(2).children.mark_style = 0;
f.children.children(2).children.mark_foreground = 5;
f.children.children(3).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(3).children.foreground = 13;
f.children.children(3).children.mark_style = 0;
f.children.children(3).children.mark_foreground = 13;


//f.children.children(7).children.thickness = 3;
//f.children.children(8).children.thickness = 3;

g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 1;  //Schriftgröße

//Labels '$' in Latex-Schreibweise
xlabel('$\text{Zeit/min}$', 'fontsize', 2);
ylabel('$\Delta\text{T/K}$', 'fontsize',2);


xs2pdf(0,"graphic_output/thesis_platten.pdf");

