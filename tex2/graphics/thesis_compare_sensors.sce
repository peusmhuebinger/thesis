// thesis_compare_sensors

//Thesis Versuch 1 -> Temperatur an Kathode und Anode

clc;clear; 

//Funktionsaufruf
exec('csv_read_conv_functions.sci')

//Korrektes Einlesen des csv-files
fileName1 = "csv_files/thesis_compare_sensors.csv"

[timeValues1, measurementValue1]=read_grafana_csv(fileName1);


//Normierung Zeitwert in Sekunde(1)/Minute(60)/Stunde(3600)
t1 = timeValues1/60; //1Wire außen


//Einlesen von Außen Sensoren
y101 = measurementValue1(:,1) 
y102 = measurementValue1(:,2)
y103 = measurementValue1(:,3)
y104 = measurementValue1(:,4)
y105 = measurementValue1(:,5)

delta_y101 = (y101-y105);
delta_y102 = (y102-y105);
delta_y103 = (y103-y105);
delta_y104 = (y104-y105);
y_sum = (y101+y102+y103+y104)/4;
y_diff = y_sum - y105;
y_diff2 = y103-y105;

//Graphik - Einstellungen
//figure(4);
subplot(311)


xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(t1,y103,3);
plot2d(t1,y101,5);
plot2d(t1,y105,1);
plot2d(t1,y102,4);
plot2d(t1,y104,7);
//plot2d(t1,delta_y101,8);
//plot2d(t1,delta_y102,8);
//plot2d(t1,delta_y103,8);
//plot2d(t1,delta_y104,8);
//plot2d(t1, y105);
//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Zellensensor1}$","$\text{Zellensensor2-4}$","$\text{Packsensor}$",[4])

// Layout für TEX Designs
f = gcf();
f.figure_size = [431,500];
f.axes_size = [425,470];
f.children.font_size = 0.5;
f.children.thickness = 0.5;

f.children.children(2).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(2).children.foreground = 13;
f.children.children(2).children.mark_style = 0;
f.children.children(2).children.mark_foreground = 13;
f.children.children(3).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(3).children.foreground = 13;
f.children.children(3).children.mark_style = 0;
f.children.children(3).children.mark_foreground = 13;
f.children.children(4).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(4).children.foreground = 5; //Polyline features (Dicke der Linien)
f.children.children(4).children.mark_style = 0;
f.children.children(4).children.mark_foreground = 5;
f.children.children(5).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(5).children.foreground = 13; //Polyline features (Dicke der Linien)
f.children.children(5).children.mark_style = 0;
f.children.children(5).children.mark_foreground = 13;
f.children.children(6).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(6).children.foreground = 1; //Polyline features (Dicke der Linien)
f.children.children(6).children.mark_style = 0;
f.children.children(6).children.mark_foreground = 1;

//f.children.children(7).children.thickness = 3;
//f.children.children(8).children.thickness = 3;

g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 1;  //Schriftgröße

//Labels '$' in Latex-Schreibweise
ylabel('$\text{T/}^\circ\text{C}$', 'fontsize',2);


subplot(312);


xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(t1, y_diff);


//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Abweichung vom arith. Mittel}$",[4])

// Layout für TEX Designs
f = gcf();
f.figure_size = [431,500];
f.axes_size = [425,470];
f.children.font_size = 0.5;
f.children.thickness = 0.5;

f.children.children(2).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(2).children.foreground = 5;
f.children.children(2).children.mark_style = 5;
f.children.children(2).children.mark_foreground = 5;


//f.children.children(7).children.thickness = 3;
//f.children.children(8).children.thickness = 3;

g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 1;  //Schriftgröße

//Labels '$' in Latex-Schreibweise
ylabel('$\Delta\text{T/K}$', 'fontsize',2);

subplot(313);


xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(t1, y_diff2);


//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{max. Abweichung}$",[4])

// Layout für TEX Designs
f = gcf();
f.figure_size = [431,600];
f.axes_size = [425,570];
f.children.font_size = 0.5;
f.children.thickness = 0.5;

f.children.children(2).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(2).children.foreground = 5;
f.children.children(2).children.mark_style = 5;
f.children.children(2).children.mark_foreground = 5;


//f.children.children(7).children.thickness = 3;
//f.children.children(8).children.thickness = 3;

g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 1;  //Schriftgröße

//Labels '$' in Latex-Schreibweise
xlabel('$\text{Zeit/min}$', 'fontsize', 2);
ylabel('$\Delta\text{T/K}$', 'fontsize',2);


xs2pdf(0,"graphic_output/thesis_compare_sensors.pdf");
