//NtTC und 1-Wire Sensoren Ausleser

clc;clear; 

//Funktionsaufruf
exec('csv_read_conv_functions.sci')

//Korrektes Einlesen des csv-files
fileName1 = "csv_files/grafana_data_sc_line1.csv"
fileName2 = "csv_files/grafana_data_sc_line2.csv"

[timeValues1, measurementValue1]=read_grafana_csv(fileName1);
[timeValues2, measurementValue2]=read_grafana_csv(fileName2);

//Normierung Zeitwert in Sekunde(1)/Minute(60)/Stunde(3600)
t1 = timeValues1/60; //NTCs
t2 = timeValues2/60; //1Wire

//Kalibrierwert
umgebung = measurementValue2(:,17);
//Einlesen von 8 y-Werten NTC
y11 = measurementValue1(:,1); 
y12 = measurementValue1(:,2);
y13 = measurementValue1(:,3);
y14 = measurementValue1(:,4);
y15 = measurementValue1(:,5);
y16 = measurementValue1(:,6);
y17 = measurementValue1(:,7);
y18 = measurementValue1(:,8); 
y19 = measurementValue1(:,9);
y110 = measurementValue1(:,10);
y111 = measurementValue1(:,11);
y112 = measurementValue1(:,12);
y113 = measurementValue1(:,13);
y114 = measurementValue1(:,14);
y115 = measurementValue1(:,15); 
y116 = measurementValue1(:,16);
y_balken = ones(666,1)*45;
y_mittel = (y11+y12+y13+y14+y15+y16+y17+y18+y110+y111+y112+y113+y114+y115+y116)/15;



///////////////////////////////////////////GRAPH 1 /////////////////////////////////////////////////////

//Graphik - Einstellungen
//figure(4);
scf(0); //Nummer des Graphik-Fensters
clf(); //Löschen des Graphikhintergrunds
xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(t1,y_mittel);
plot2d(t1,y19);
plot2d(t2,umgebung);
plot2d(t1,y_balken,color("grey"));
//plot2d(t1,y12);
//plot2d(t1,y13);
//plot2d(t1,y14);
//plot2d(t1,y15);
//plot2d(t1,y16);
//plot2d(t1,y17);
//plot2d(t1,y18); 
//plot2d(t1,y110);
//plot2d(t1,y111);
//plot2d(t1,y112); 
//plot2d(t1,y113);
//plot2d(t1,y114);
//plot2d(t1,y115);
//plot2d(t1,y116);
  

//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{arith. Mittel übrige Sensoren}$","$\text{Sensor9}$","$\text{Sensor Umgebung}$","$\text{max. zul. Betriebs T.}$",[2])


// Layout für PNG Designs
f = gcf();
f.figure_size = [600,360];
f.axes_size = [550,340];
f.children.font_size = 0.5;
f.children.thickness = 0.5;

f.children.children(2).children.thickness = 3;
f.children.children(2).children.line_style = 8;
f.children.children(3).children.thickness = 1;
f.children.children(3).children.foreground = 13;
f.children.children(3).children.mark_style = 10;
f.children.children(3).children.mark_foreground = 13;
f.children.children(4).children.thickness = 1;
f.children.children(4).children.foreground = 5;
f.children.children(4).children.mark_style = 10;
f.children.children(4).children.mark_foreground = 5;
f.children.children(5).children.thickness = 1;
f.children.children(5).children.foreground = 1;
f.children.children(5).children.mark_style = 10;
f.children.children(5).children.mark_foreground = 1;


g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 1;  //Schriftgröße

//Labels '$' in Latex-Schreibweise
xlabel('$\text{Zeit/min}$', 'fontsize', 2);
ylabel('$\text{T/}^\circ\text{C}$', 'fontsize',2);

xs2pdf(0,"graphic_output/sc_1.pdf");

