clc;

dat = readxls('excel_test_doc.xls');
datemenge = dat(1);
stats = datenmenge.value

t = stats(:,1);
y = stats(:,2);


//Plotten
scf(3);
clf();
xgrid(0);
plot2d(t,y);



xlabel('$\text{Test X-Achse}$', 'fontsize', 3);
ylabel('$\text{Test Y-Achse}$', 'fontsize',3);

xs2pdf(0,"peus3.pdf");
