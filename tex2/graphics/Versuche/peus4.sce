clc;

dat = csvRead("excel_test_doc.csv",";");


t = dat(:,1);
y = dat(:,2);


//Plotten
scf(4);
clf();
xgrid(0);
plot2d(t,y);



xlabel('$\text{Test X-Achse}$', 'fontsize', 3);
ylabel('$\text{Test Y-Achse}$', 'fontsize',3);

xs2pdf(0,"peus4.pdf");

