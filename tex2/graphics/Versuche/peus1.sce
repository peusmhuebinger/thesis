clc;

dat = fscanfMat("excel_test_doc.dat");

t = dat(:,1);
y = dat(:,2);

//Plotten
scf(0);         // <---- hier Nummer der Graphik wählen scf(x)
clf();
//clf();
xgrid(0);
plot2d(t,y);



xlabel('$\text{Test X-Achse}$', 'fontsize', 3);     //xlabel Namen ändern
ylabel('$\text{Test Y-Achse}$', 'fontsize',3);      //ylabel Namen ändern


xs2pdf(0,"peus1.pdf");                             //Name pdf ändern
