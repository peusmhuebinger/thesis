clc;


x = [1:4]
y = [1:4]
tmp_innen = [20.06 30.75 20.88 18.81; 21.13 42.56 24.38 18.94; 19.94 28.63 22.31 19.06; 18.69 19.44 19.13 18.75]
tmp_außen = [19.75 22.38 21.75 19.5; 20 28.13 27.31 19.75; 19.38 22.81 23.19 19.56; 19.25 19.44 19.81 19.31]

scf(2);

subplot(211);

f=gcf();
f.color_map = jetcolormap(128)
f.children(1).font_size = 3;
f.children(1).z_ticks.locations=matrix 3x1;
f.children(1).z_ticks.labels=["20";"30";"40"];


title("$\text{Innerer Temperaturverlauf der Sensoren nach einer Stunde}$",'fontsize',5)
zlabel("$\text{Temperatur in }^\circ\text{C}$",'fontsize',4)

surf(x,y,tmp_innen,'edgeco','b','marker','d','markersiz',9,'markeredg','red','markerfac','k')
e=gce();
e.color_flag=1 // color index proportional to altitude (z coord.)
e.color_flag=2; // back to default mode
e.color_flag= 3; // interpolated shading mode (based on blue default color because field data.color is not filled)

subplot(212);
f=gcf();
f.color_map = jetcolormap(128)
f.children(1).font_size = 3;

title("$\text{Äußerer Temperaturverlauf der Sensoren nach einer Stunde}$",'fontsize',5)
zlabel("$\text{Temperatur in }^\circ\text{C}$",'fontsize',4)

surf(x,y,tmp_außen,'edgeco','b','marker','d','markersiz',13,'markeredg','red','markerfac','k')
e=gce();
e.color_flag=1 // color index proportional to altitude (z coord.)
e.color_flag=2; // back to default mode
e.color_flag= 3; // interpolated shading mode (based on blue default color because field data.color is not filled)

xs2pdf(2,"graphic_output/3d_view.pdf");
