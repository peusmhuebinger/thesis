//Test3 -> 1-Wire in Kammern und oben drauf

clc;clear; 

//Funktionsaufruf
exec('csv_read_conv_functions.sci')

//Korrektes Einlesen des csv-files
fileName1 = "csv_files/grafana_data_export_test3.csv"

[timeValues1, measurementValue1]=read_grafana_csv(fileName1);


//Normierung Zeitwert in Sekunde(1)/Minute(60)/Stunde(3600)
t1 = timeValues1/3600; //1Wire außen

//Kalibrierwert
calibration = measurementValue1(:,17);
//Einlesen von Außen Sensoren
y101 = measurementValue1(:,1) - calibration; 
y102 = measurementValue1(:,2) - calibration;
y103 = measurementValue1(:,3) - calibration;
y104 = measurementValue1(:,4) - calibration;
y105 = measurementValue1(:,5) - calibration;
y106 = measurementValue1(:,6) - calibration;
y107 = measurementValue1(:,7) - calibration;
y108 = measurementValue1(:,8) - calibration; 
y109 = measurementValue1(:,9) - calibration;
y110 = measurementValue1(:,10) - calibration;
y111 = measurementValue1(:,11) - calibration;
y112 = measurementValue1(:,12) - calibration;
y113 = measurementValue1(:,13) - calibration;
y114 = measurementValue1(:,14) - calibration;
y115 = measurementValue1(:,15) - calibration;
y116 = measurementValue1(:,16) - calibration;

y_sum1 = (y109+y110+y111+y112)/4;
y_diff1 = y_sum1 - y103;



//Graphik - Einstellungen
//figure(4);
subplot(211);
xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot(t1,y103);
plot(t1,y109);
plot(t1,y110);
plot(t1,y111);
plot(t1,y112);
//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Sensor 3}$","$\text{Sensor 9}$","$\text{Sensor 10}$","$\text{Sensor 11}$","$\text{Sensor 12}$",[2])

// Layout für PNG Designs
f = gcf();
f.figure_size = [431,360];
f.axes_size = [425,340];
f.children.font_size = 0.5;
f.children.thickness = 0.5;

f.children.children(2).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(2).children.foreground = 5;
f.children.children(2).children.mark_style = 5;
f.children.children(2).children.mark_foreground = 5;
f.children.children(3).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(3).children.foreground = 3;
f.children.children(3).children.mark_style = 3;
f.children.children(3).children.mark_foreground = 3;
f.children.children(4).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(4).children.foreground = 13; //Polyline features (Dicke der Linien)
f.children.children(4).children.mark_style = 13;
f.children.children(4).children.mark_foreground = 13;
f.children.children(5).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(5).children.foreground = 6;
f.children.children(5).children.mark_style = 6;
f.children.children(5).children.mark_foreground = 6;
f.children.children(6).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(6).children.foreground = 4; //Polyline features (Dicke der Linien)
f.children.children(6).children.mark_style = 4;
f.children.children(6).children.mark_foreground = 4;
//f.children.children(7).children.thickness = 3;
//f.children.children(8).children.thickness = 3;

g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 1;  //Schriftgröße

//Labels '$' in Latex-Schreibweise
ylabel('$\Delta\text{T/K}$', 'fontsize',2);

subplot(212);

xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(t1,y_diff1,4);
//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Abweichung Sensor 3 vom arith. Mittel}$",[1])

// Layout für PNG Designs
f = gcf();
f.figure_size = [431,360];
f.axes_size = [425,340];
f.children.font_size = 0.5;
f.children.thickness = 0.5;

f.children.children(2).children.thickness = 2; //Polyline features (Dicke der Linien)
f.children.children(2).children.foreground = 5;
f.children.children(2).children.mark_style = 5;
f.children.children(2).children.mark_foreground = 5;


g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 1;  //Schriftgröße

//Labels '$' in Latex-Schreibweise
ylabel('$\Delta\text{T/K}$', 'fontsize',2);
xlabel('$\text{Zeit/h}$', 'fontsize', 2);


xs2pdf(0,"graphic_output/thesis_sensorverteilung2.pdf");
