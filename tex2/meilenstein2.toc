\contentsline {chapter}{\numberline {1}Einleitung}{1}
\contentsline {chapter}{\numberline {2}Annahmen zur Temperaturregelung}{2}
\contentsline {section}{\numberline {2.1}DIN VDE 62619}{2}
\contentsline {section}{\numberline {2.2}DIN VDE 62619: \"Uberhitzungskontrolle}{2}
\contentsline {section}{\numberline {2.3}Anforderungen an Messsystem}{3}
\contentsline {chapter}{\numberline {3}Platzierung der Sensoren}{4}
\contentsline {section}{\numberline {3.1}\"Uberlegungen zur Platzierung}{4}
\contentsline {section}{\numberline {3.2}Versuchsaufbau: Platzierung der Sensoren innen und au\IeC {\ss }en}{5}
\contentsline {section}{\numberline {3.3}Messergebnisse: Platzierung der Sensoren innen und au\IeC {\ss }en}{5}
\contentsline {section}{\numberline {3.4}Versuchsaufbau: Sensorplatzierung innen}{6}
\contentsline {section}{\numberline {3.5}Messergebnisse: Sensorplatzierung innen}{6}
\contentsline {chapter}{\numberline {4}Fazit}{9}
\contentsline {chapter}{\numberline {5}Ausblick: Implementierung}{10}
