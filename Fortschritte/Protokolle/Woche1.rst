Woche 1 - 25.09-18 --- 27.09.18
===============================

Motivation der Arbeit:
----------------------

Die Temperatur ist einer der wichtigsten Einflussparameter auf die Performance von Batteriesystemen. Gerade in Lithium - Ionen Akkumulatoren
kam es in der zurückliegenden Zeit zahlreiche Fälle von problematischen Störungen im Zusammenhang mit dem thermischen Verhalten der Zellen.

Thermik der Batterie-Zelle Typ LiFePO4
--------------------------------------

Allgemeines
^^^^^^^^^^^^

LiFePO4 besitzt den Vorteil enge kovalenter Bindungen seiner Sauerstoffatome. Das bedeutet zum einen, dass sich diese durch die schwierige Löslichkeit
nicht in ausdehnendes Gas umwandeln, zum einen unterbindet es einen frühen thermal runaway, dem thermischen "Durchgehen" der Zelle.
Somit besitzt das Material eine höhe Explosions- und Ausbrennsicherheit als Li-Ionen Zellen anderer chemischer Zusammensetzung.
Graphik :numref:`heat_temp` zeigt den Punkt des "thermal runaway" bei LiFePO4 - Zellen, an dem die Hitze unkontrolliert steigt und die Zelle anfängt zu brennen.  

.. _heat_temp:
.. figure:: Bilder/heat_temp_relation.png
    :name: heat
    :alt: heat
    :align: center

    *Kathodische und anodische Hitzeabstrahlung in W/g über der Temperatur. Peak c zeigt die Zersetzung der SEI, Peak d den Punkt des thermal runaway* [#FN1]_

Dennoch haben die unterschiedlichen Betriebstemperaturen unterschiedlich starke Wirkungen auf die LiFePO4 Zellen. Tiefe Temperaturen führen zu geringen 
Zell-Innenwiderständen, die dafür sorgen, dass der Akkumulator eine geringere Kapazität besitzt. Akkumulatoren besitzen bei -20°C lediglich eine Kapazität
von 60% der Kapazität, die sie bei Raumtemperatur von ca. 25 °C haben. 
Schaubild :numref:`cap_temp` zeigt die Kapazitätsverluste bei einer Temperatur unter 15 °C deutlich. 

.. _cap_temp:
.. figure:: Bilder/cap_temp_relation.png
    :name: cap
    :alt: cap
    :align: center

    *Verfügbare Kapazität über der Temperatur* [#FN2]_

Zu hohe Temperaturen wiederum wirken sich nachteilig auf die Alterung der Zelle aus. Nach 2000 Zyklen verlieren die Zellen bei 40°C 20% ihrer Ausgangskapazität,
bei höheren Temperaturen sind die Kapazitätsverluste sehr viel höher, 30% Verlust bei 50°C und 45% Verlust bei 60°C.
Schaubild :numref:`cap_cycle` zeigt die Kapazitätsverluste nach einer gewissen Zyklenzahl.  

.. _cap_cycle:
.. figure:: Bilder/cap_cycle_relation.png
    :name: cycle
    :alt: cycle
    :align: center

    *Kapazitätsverlust über eine Zyklenzeit von bis zu 2000 Zyklen bei Temperaturen von 10°C (283K) bis 60°C (333K)* [#FN3]_  

Die Norm DIN EN 62619
^^^^^^^^^^^^^^^^^^^^^^

Die neue Norm (11.2017) zu den Sicherheitshinweisen im industriellen Umgang mit Lithium-Ionen Akkumulatoren ist die DIN EN 62619.
In ihr gilt: "In jedes Batteriesystem müssen sie eine unabhängige Steuerung und ein Schutzverfahren integrieren und gegebenenfalls weitere 
Schutzschaltungen einbauen. Werden Speicherzellen in Reihe zusammengeschalten, hat der Hersteller dafür zu sorgen, dass man die Anordnung nicht 
verändern und die Zellen umpolen kann. Bei der Prüfung des Speichergeräts ist ein Bericht gemäß der Norm zu erstellen. Damit Anwender die 
Speichersysteme richtig gebrauchen, müssen die Hersteller von vornherein Grenzwerte für die Ladespannung, den Ladestrom und Ladetemperatur 
festlegen. Allgemein sind Lithium-Akkumulatoren zur Anwendung in einer Umgebung zwischen 10 und 45 Grad Celsius empfohlen. Dieser kann je 
nach verwendeten Materialien aber variieren." [#FN4]_

Thermisches Profil der Zelle 26650 von A123
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Die 26650er LiFePO4 Akkumulator-Zelle von A123 verfügt über eine Kapazität von 2,5 Ah. Sie besitzt eine Leerlaufspannung wie herkömmliche Li-Ionen Akkus 
von rund 3,2 V. Der maximale Entladestrom vom Hersteller empfohlen, beträgt 5C, also 12,5 A. Der Innenwiderstand der Zelle soll < 6 mOhm sein, Tests belegen
einen realistischen Wert von ca. 4 - 6,5 mOhm.

**Thermisches Profil experimentell**

Um die Temperaturmessung der Zelle besser zu verstehen, erfordert es das thermische Verhalten bei Belastung und Umgebungstemperatur genauer zu betrachten.
Für die Temperaturerfassung der 26650er Zelle wird die Batterie auf unterschiedliche Entladeströmen belastet. 
Zur Messung werden drei unterschiedliche Messsensoren benutzt, um das Ergebnis möglichst korrekt darzustellen. Über die Messungen können die 
thermischen Widerstände der Kathode, der Anode und der Zellwand erstellt werden und so der Punkt der größten Wärmeentwicklung entdeckt werden.
Schaubild :numref:`bat_therm`/:numref:`bat_therm2` zeigt das vereinfachte thermische Ersatzschaltbild der Batterie-Zelle. Drei unterschiedliche Wärmequellen -> Anode, Kathode und Zellwand
besitzen unterschiedliche thermische Widerstände bezogen auf die Umgebungstemperatur. 

.. _bat_therm:
.. figure:: Bilder/bat_therm.png
    :name: bat_t
    :alt: bat_t
    :align: left

    *Thermisches Ersatzschaltbild der A123 - Zelle*  

.. _bat_therm2:
.. figure:: Bilder/thermic_model.png
    :name: bat_t2
    :alt: bat_t2
    :align: right

    *Thermisches Ersatzschaltbild der A123 - Zelle*  

Im folgenden Versuch wird nun eine Messreihe zur Bestimmung des thermischen Verhaltens der Zelle ermittelt. Drei verschiedene Zellen - neu, 2,5 Ah / alt, 2,5 Ah und alt und schwache Kapazität 2,2 Ah - 
werden auf ihre thermischen Eigenschaften überprüft. Alle Zellen werden unterschiedlichen Entladeströme ausgesetzt, zuerst 10A, dann der vom Hersteller
vorgegebene Grenzwert von 12,5 A und anschließend 15 A. Das thermische Verhalten an Kathode, Anode und Zellwand wird anhand einer Wärmebildkamera 
aufgezeichnet. Daraus werden dann die thermischen Widerstände der Zellen ermittelt. 

**10 A - Test**

Annotation:

- Zelle 1 --- KOK 2,2 Ah    (alt)
- Zelle 2 --- KOK 2,5 Ah    (alt)
- Zelle 3 --- A123 2,5 Ah   (neu)

*Die Tests erfolgen bei einer Umgebungstemperatur von T_Umgebung = 23°C*

+---------------+-------------------------+-------------------------------+
| Zellennummer  | Spitzentemperatur in °C | thermischer Widerstand in K/W |
+===============+=========================+===============================+
|            1  |                     46  |    23K / 0,4335= 53,05 K/W    |
+---------------+-------------------------+-------------------------------+
|            2  |                     39  |    16K / 0,337 = 47,47 K/W    |
+---------------+-------------------------+-------------------------------+
|            3  |                     41  |    18K / 0,315 = 57,14 K/W    |
+---------------+-------------------------+-------------------------------+


Zelle1: 8,5^2  * 6 = 433,5   mW

Zelle2: 7,5^2  * 6 = 337,5   mW

Zelle3: 7,25^2 * 6 = 315,4   mW


.. note:: *Die folgenden Tests wurden mit einer Umgebungstemperatur von T_Umgebung = 21°C durchgeführt und an der Kathode der 26650 Zelle gemessen*

Im folgenden Test wird die Batterie - Zelle bis 50°C erwärmt, bei einem Strom von 8C (20A). Nach Erreichen der Spitzentemperatur bei
ca. 4,5 V, wird die Zelle vom Netzteil getrennt und der Abkühlungsvorgang aufgezeichnet. 
:numref:`erw_abk` zeigt den graphischen Verlauf.

.. _erw_abk:
.. figure:: Bilder/erw_abk.png
    :name: ea
    :alt: ea
    :align: center

    *Thermisches Profil der 26650 - Zelle bei Erwärmung und Abkühlung*  

Der Erwärmungsvorgang dauert nur ca. 7 Minuten, während das komplette Runterkühlen auf die Ausgangstemperatur ca. 1 Stunde benötigt.

Die folgende Abbildung zeigt nun die Erwärmung bis auf eine fest eingestellte Ladeschlussspannung von 3,6 V. Die Zellen werden mit
unterschiedlichen Ladeströmen belastet, 4C (10A), 5C (12,5A) und 8C (20A).
:numref:`xls_lade_458C` zeigt das Ergebnis.

.. _xls_lade_458C:
.. figure:: Bilder/xls_lade_458C.png
    :name: xlsl
    :alt: xlsl
    :align: center

    *Erwärmung der 26650 - Zelle für unterschiedliche Ladeströme*  

Das Ergebnis zeigt, dass keine der Zellen eine Temperatur von 50°C überschreitet.
Die Profile der thermischen Impedanzen für verschiedene Ströme (:numref:`xls_w_kap`) wiederum zeigen, dass die Wärmekapazität zunimmt. 

.. _xls_w_kap:
.. figure:: Bilder/xls_w_kap.png
    :name: xlsw
    :alt: xlsw
    :align: center

    *Thermischer Impedanzeverlauf für diverse Ströme*  

Die mathematische Herleitung (:numref:`mat`) zeigt, dass für den Verlauf der thermischen Impedanzen, die thermischen Kapazitäten verantwortlich
sind. Mt der Annahme, dass die thermischen Widerstände der Zellen gleich bleiben, sieht man eine Erhöhung der thermischen Kapazitäten für größere
Ströme, da die zugeführte Energie proportional mit dem Strom steigt. Abbildung :numref:`bat_therm2` zeigt, dass Z_th = R_th || C_th ist. Das führt 
dazu, dass, da R_th konstant angenommen werden kann, die thermischen Impedanzen für größere Ströme sinken.  

.. _mat:
.. figure:: Bilder/mat_her.png
    :name: mat_her
    :alt: mat_her
    :align: center














.. [#FN1] Bildquelle: "Thermal runaway features of 18650 lithium-ion batteries for LiFePO4 cathode material by DSC and VSP2" by Wen, Jhu, Wang, Chiang, Shu,
.. [#FN2] Bildquelle: "LiFePO4 battery performances testing and analyzing for BMS" by Dr. Languang Lu
.. [#FN3] Bildquelle: "LiFePO4 Optimal Operation Temperature Range Analysis for EV/HEV" by Sun, Yang, Lu, Wei, Zhu
.. [#FN4] Quelle: https://www.photovoltaikforum.com/magazin/praxis/sicherheitsnorm-fuer-lithium-batterien-der-warteschleife-2213/ zur Norm 62619 