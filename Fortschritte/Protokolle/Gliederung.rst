Gliederung
==========

**1. Einleitung des Themas:** Darstellung der Aufgabe der Temperaturmessung, Motivation, Normen, Thermik der Zelle -> knappe Darstellung der Ergebnisse

**2. Inhaltsverzeichnis**

**3. Aufgabe Themenschwerpunkt 1: Temperaturmessung** -> Erklärung Aufgabe

    *3.1 Lösungsfindung, Evaluaierung der Möglichkeiten -> Sensoren, Schnittstellen, uC*
    
    *3.2 Beschreibung des Ergebnisses*

**4. Aufgabe Themenschwerpunkt 2: thermische Verteilung im Pack**
    
    *4.1 Darstellung der Lösung, thermisch betrachtete Lösungsfindung* *(therm. Fotographie)*

**5. Ergebniszusammenstellung, Bewertung der Arbeit, kritischer Rückblick**

**6. Literaturverzeichnis und Anhänge**

|

|


Wochenplan
==========

---

Woche 1 - 25.09 - 27.09: Vorbereitung thermische Entwicklung Batteriezellen    

Woche 2 - 01.10 - 05.10: Tests an einer Batteriezelle, Tests im Pack mit Heizelement, thermische Verteilung. Tests der Sensoren im Pack

Woche 3 - 08.10 - 12.10: Messreihe thermodynamische Verteilung des BatteryPack

Woche 4 - 15.10 - 19.10: **Absenden erste Präsentation 15.10** / **Präsentation 17.10** / 1-Wire Programmierung / Docs lesen des I2C-Slaves, erste Programmierung 

Woche 5 - 22.10 - 26.10: I2C slave - 1-Wire Programmierung

Woche 6 - 29.10 - 02.11: **Wolfrum 29.10** / I2C slave - 1-Wire Programmierung

Woche 7 - 05.11 - 09.11: Thermoverteilung und Aufbau Sensoren

Woche 8 - 12.11 - 16.11: Thermische Verteilung 1-Wire auf und im Pack/Überlegungen Kurzschlusstest

Woche 9 - 19.11 - 23.11:

Woche 10 - 26.11 - 30.11:

Woche 11 - 03.12 - 07.12:

Woche 12 - 10.12 - 14.12:

Woche 13 - 17.12 - 21.12:

---

Frei                     -> Erstentwurf der Bachelorarbeit

---

Woche 14 - 07.01 - 11.01:

Woche 15 - 14.01 - 18.01:

Woche 16 - 21.01 - 25.01:

Woche 17 - 28.01 - 02.02:

Woche 18 - 05.02 - 09.02:

Woche 19 - 12.02 - 16.02:

Woche 20 - 19.02 - 23.02:

Woche 21 - 26.02 - 28.02: späteste Abgabe


