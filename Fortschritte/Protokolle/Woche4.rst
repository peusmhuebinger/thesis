Woche 2 29.10.18 --- 02.11.18
==============================

Auswertung der NTC-Versuche
----------------------------

Die Verteilung der NTC - Sensoren soll einen möglichst guten Überblick über die Wärmeverteilung geben
und gleichzeitig Kombinationen für die Anbringung der Sensoren erproben.

Versuchsaufbau
---------------

siehe Präsentation Anfang November