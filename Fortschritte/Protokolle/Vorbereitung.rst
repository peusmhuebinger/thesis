Vorbereitung
============

Messung der Temperatur und Betrachtung der Wärmeausdehnung im Batteriepack
--------------------------------------------------------------------------

Vorraussetzungen:   

- mind. 1(-2)x Messung der Temperatur in der Sekunde
- I2C Kommunikation mit überschaubarer Anzahl an Befehlen
- Nutzung von 1-Wire Temperatursensoren

Möglichkeiten:
--------------

**DS2482 – 100 Chip**

Der DS2482-100 chip ist ein I2C-to-1Wire bridge device. Er ist ein I2C slave für die Datenübertragung in standard (100 kHz) oder schneller (400kHz) 
Geschwindigkeit zu einem I2C Master. 
Er fungiert selbst als 1-Wire Master, für die Kommunikation von vielzähligen 1-Wire Slaves. Das Blockschaltbild ist in :numref:`ds2482_block` dargestellt. 

.. _ds2482_block:
.. figure:: Bilder/ds2482_block.png 
    :name: ds2482
    :alt: ds2482
    :align: center

    *Blockschaltbild des DS2482 - Aufbaus*
 

**1-Wire device: Temperatursensor MAX31820PAR**

Der MAX31820PAR ist ein Umgebungstemperatursensor mit einer Genauigkeit von +- 0,5 °C in einem Temperaturbereich von 10-45 °C. 
Der Sensor kommuniziert mit einem Host über den sogenannten 1-Wire Bus. Dieser ist, wie der Name schon sagt, mit nur einer Datenleitung 
ausgerüstet. Der Sensor benötigt keine externe Spannungsversorgung. 

Der Sensor ist ein sogenannter „parasitärer“ Sensor, er arbeitet auch ohne externe Spannungsversorgung. 
Über einen „pullup“ Widerstand bezieht der Sensor seine Spannung wenn die Datenleitung auf „high“ ist. Das Bussignal lädt 
einen internen Kondensator des Sensors auf, der den Sensor mit Leistung versorgt, auch wenn die Busleitung dann auf „low“ liegt. 

Die Wärmeübertragung des 31820 erfolgt über die Kontakte des Sensors, die die Wärme zum Sensorkopf trägt. Die Isolierung
des Sensorgehäuses verhindert, dass die zu messende Wärme über das Gehäuse entweicht. Deswegen ist es wichtig, den Sensor 
bei der Messung nahe an die zu messende Wärmequelle heranzubringen. Das kleine und robuste Gehäuse lässt sich damit auch 
gut in enge Anwendungen implementieren. 

Die Temperaturmessung erfolgt über eine Temperaturfühler-IC, das den analogen Messwert direkt digital verarbeitet und 
auf einen internen Speicherbaustein schreibt. Die Genauigkeit der Messung kann dabei zwischen einer Auflösung von 9 bis 
12 bit liegen (0,5°C, 0,25°C, 0,125°C, 0,0625°C). Um eine Temperaturmessung umzusetzen und zu digitalisieren, muss der Sensor 
den Convert temperature (0x44h) Befehl bekommen. 

Schaubild :numref:`max31820par_block` zeigt das Blockschaltbild eines MAX31820PAR.

.. _max31820par_block:
.. figure:: Bilder/max31820par_block.png 
    :name: maxpar
    :alt: maxpar
    :align: center

    *Interner Aufbau des MAX31820PAR mit Registern*
  
Neben dem erwähnten Anschluss für den Kondensator, der den Sensor mit Strom versorgt, besitzt der Temperatursensor 
eine unique mechanische Adresse, die 64 bit groß ist. Mit dieser einzigartigen Adresse kann der Sensor als 1-Wire device 
alleine angesprochen werden. Dahinter befindet sich die Datenstruktur, das sogenannte Scratchpad. Dies ist die Registerstruktur 
des Temperatursensors und beinhaltet den digital umgewandelten Temperaturwert, unterteilt in most significant bit und least 
significant bit (0x00 LSB/ 0x01 MSB). Register 0x04 beinhaltet den Conversion Wert, der sich aus folgender Bitstruktur (:numref:`conv_register`) zusammensetzt. 

.. _conv_register:
.. figure:: Bilder/conv_register.png 
    :name: conv
    :alt: conv
    :align: center

    *Zusammensetzung des Conversion Registers*

Bit 5 (R0) und Bit 6 (R1) legen die Auflösungsgröße der Temperatur fest. 
Als letzter Register Eintrag 0x08 ist der CRC zu nennen, der die fehlerfreie Übertragung der Daten überprüfen soll. 
