Woche 2 01.10.18 --- 05.10.18
===============================

Thermische Ausdehnung durch Heizelement in Pack
-----------------------------------------------

Der folgende Versuch besteht darin, die Ausdehnung der Erwärmung im Batterie - Pack
zu untersuchen. Dafür wird der Pack erst mit einem Heizelement bestückt und dieser sukzessive erwärmt.
Die Verteilung der Erwärmung und Abkühlung wird im Pack erst ohne und anschließend mit Batterie-Zellen 
untersucht. Die thermische Verteilung wird erneut durch die Wärmekamera aufgezeichnet.

.. _heiz_pro:
.. figure:: Bilder/heiz_pro.png
    :name: heiz
    :alt: heiz
    :align: center

    *Vorgehen der Platzierung des Heizelements im Batterie - Pack, mit und ohne Zellen*  

:numref:`heiz_pro` zeigt das Vorgehen mit und ohne Batterie - Zellen. 

Der erste Versuch wurde ohne Abeckung gemessen. So konnte die Wärmebildkamera die thermische Verteilung 
im Pack aufzeichnen. :numref:`V1` zeigt die Messung des Heizelements nur im Pack (links) und die Messung mit
einkreisender Anzahl an Zellen (rechts). 

.. _V1:
.. figure:: Bilder/V1.jpg
    :name: heiz
    :alt: heiz
    :align: center

    *offenes Batteriepack mit Heizelement (links) und Zellen, die das Heizelement einkreisen (rechts)*

Das Heizelement wird bei beiden Versuchsaufbauten auf 155°C erhitzt und alle 10 Minuten mit der Wärmekamera betrachtet. 
:numref:`ohne` zeigt unmittelbar nach Entnahme des Heizelements aus dem Pack, dass sich die äußeren Ränder einer Batterieöffnung im Pack erwärmt haben.
Die Wärme hat sich im Pack nahezu gar nicht verteilt und blieb lokal.

.. _ohne:
.. figure:: Bilder/Image_H_ohne_0.png
    :name: ohne
    :alt: ohne
    :align: center

    *Bild der Thermokamera: thermische Verteilung Heizelement im offenen Pack*

Auch die Zunahme von Batteriezellen erwärmte das Pack beinahe gar nicht. :numref:`mit` zeigt das Resultat mit Batteriezellen.
Die Zelle in der Mitte oben hatte als einzige direktem Kontakt zum Heizelement, deswegen ist die Erwärmung an dieser Stelle mit 39,7°C am drastischsten.
Die Eckzellen ganz außen haben sich durch die eher lokale Erwärmung nur leicht erwärmt. Die Werte liegen hier nur zwischen 27-28°C. 

.. _mit:
.. figure:: Bilder/Image_H_mit.png
    :name: mit
    :alt: mit
    :align: center

    *Bild der Thermokamera: thermische Verteilung des Heizelements auf umstehende Batteriezellen*


Überarbeitung des Heizelements
-------------------------------

Die thermische Ausdehnung durch das Heizelement erfolgt aufgrund der Eigenschaften des Packs eher lokal. Tatsächliche drastische Erwärmungen ergeben
sich nur durch direkte Berührpunkte von Zellen oder Umgebungsmaterial mit dem Heizelement. Das verfälscht aber die Simulation, das Heizelement als
"kurzgeschlossene" Zelle darzustellen. Aufgrund den vielen Schrägen des Heizelements ist eine Modellierung als Zelle so sehr ungenau. 

Der Heizkörper des Elements wurde also entfernt und das Heizelement in ein zylinderförmigen Edelstahl gepresst, um die runde Wärmezirkulation, die eine
Batteriezelle abgeben würde, besser darstellen zu können. :numref:`H_Element2` zeigt das Resultat. Die batterieartigen Maße ermöglichen dem Heizelement
nun besser in die Batterieöffnung zu passen und die Wärmeabstrahlung rundherum abzugeben. 

.. _H_Element2:
.. figure:: Bilder/H_Element2.jpg
    :name: H_Element2
    :alt: H_Element2
    :align: center

    *Heizelement in "Batterieform"*

Kupfer bestellt

