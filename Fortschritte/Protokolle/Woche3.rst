Woche 2 08.10.18 --- 12.10.18
==============================

Thermisches Modell über ANSYS
------------------------------

Modellbau über Ansys, werkgetreuer Ausschnitt des Batterie Packs mit Wärmeleitkoeffizienten 
und 3D Aufbau

Bilder folgen

Thermisches Modell mit Heizelement 2.0 und Thermo-Lack
------------------------------------------------------

Problematisch bei den vorherigen Versuchen war die Größe des Heizelements. Der alte Aufbau
hatte eine Größe von rund 35 mm Länge und war somit nur knapp die Hälfte der Länge einer 26650-Zelle,
die im Pack zum Einsatz kommt. 

Der neue Aufbau des Heizelements erfolgt mit einem Loch direkt auf der Hälfte des zylindrischen Stahlkörpers. Das Heizelements wird
in das Loch eingeführt und mit zwei Gummipropfen verschlossen. Anschließend werden die oberen und unteren Kreisflächen des Zylinders mit
Gummi beklebt, um das die Zellen im Pack vor einem Kurzschluss zu schützen, der Stahl würde schließlich leiten.

Um der reflexierenden Wirkung der Blechplatten, die im Pack zur Leitung des Stroms und der Verbindung der Zellen angebracht ist, bei Betrachtung mit
der Wärmebildkamera vorzubeugen, wird ein entsprechender Thermolack auf den Blechen angebracht. Dieser ist beständig bis Temperaturen von 600°C und 
beeinflusst die thermische Leitfähigkeit des Materials kaum. 

Aufbau der NTC
--------------

Zur sensorischen Messung an Blech und Spritzguss-Form wird der Pack mit NTC - Temperatursensoren bestückt. 
Die Sensoren werden an einen ADC mit 8 Channels verbunden und über einen Widerstandsteiler ausgelesen.
Skizze A verdeutlicht das Prinzip. 

Der NTC verändert seinen Widerstand über die Temperatur entsprechend. Abhängig von diesem tritt eine abweichende
Spannung U2 über dem Temperaturwiderstand auf. Der Analog-Digital-Wandler (ADC) wandelt diese Spannung 
in ein digitales Signal (Bit 0 - 1023) um. 
Ein Microcontroller verarbeitet 


Notiz: Kurzschluss (!!!!) an Heizer

Testlauf mit NTC und unterschiedlicher Platzierung
---------------------------------------------------

Problematisch sind verschiedene Bezirke im Batterie - Pack. Mit der Wärmebildkamera wurden
verschiedene Winkel auf das Pack analysiert. Einmal Die Verteilung im Pack,
anschließend die Verteilung im auf und unter dem Pack, mit und ohne Abdeckung. 

Das Ergebnis war, dass die Verteilung der Wärme lediglich sehr lokal aufgenommen wurde. 

Der neue Test soll nun zeigen, ob sich dieses Wärmeprofil auch durch Verteilung mehrerer Wärmesensoren
erzielen lassen kann. 