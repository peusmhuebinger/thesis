Woche 5 12.11.18 --- 19.11.18
==============================

Temperaturmessung mit 1-Wire Sensoren
--------------------------------------

Für eine geeignete Messung zur Platzierung, werden nun die 1-Wire Sensoren benutzt. Diese werden an verschiedenen
Orten im und um den Pack platziert, um eine möglichst genaue Messung der Wärme an einer Hitzequelle zu detektieren.

Dafür werden verschiedene Testläufte durchgeführt

Test 1: Platzierung der Sensoren auf dem Pack und im Pack um Heizelement herum (außerhalb und innerhalb Pack)
Test 2: Platzierung der Sensoren in den Vierfachkammern und oben auf den Kammern (innerhalb Pack)
Test 3: Kurzschlusstest mit kompletter Sensorbestückung des Packs (innerhalb Pack)

Test 1 (Vergleich Sensorreihe 1Wire innen und außen Pack):
----------------------------------------------------------

.. _aufbau:
.. figure:: Bilder/Sensorverteilung_test1.png
    :name: mit
    :alt: mit
    :align: center

    *Verteilung der internen Sensoren*


.. _aufbau_oben:
.. figure:: Bilder/Sensorverteilung_test1_oben2.png
    :name: mit
    :alt: mit
    :align: center

    *Verteilung der Sensoren auf dem Pack*


:numref:`aufbau` und :numref:`aufbau_oben` zeigen die Sensorverteilung im Pack im 2D - Querschnitt. Der rote Kreis symbolisiert das Heizelement. 
Die Sensorverteilung ist jeweils bei beiden Aufbauten gleich, um die thermischen Unterschiede zwischen beiden Aufbauten zu detektieren. 
Es ist zu erwarten, dass der Anstieg an Sensor 7 am größten ist, da es am nahsten am Heizelement liegt. Ebenso ist der Wert von Sensor 8 zu relativieren, 
führt die Zuleitung, die wärmetechnisch schlecht isoliert ist, genau an diesem vorbei. 

Auswertung
***********

.. _vergleich:
.. figure:: Bilder/vergleich_3d.png
    :name: mit
    :alt: mit
    :align: center

    *3D - Graphik der thermischen Verteilung nach einer Stunde*

:numref:`vergleich` zeigt den Vergleich von der thermischen Lokalisierung im Pack. Der naheliegendste Sensor am Heizelement bestimmt den Maximalausschlag.
Man kann sehen, dass sich die Wärme oben auf dem Pack deutlicher verteilt als im Pack. Bei den inneren Sensoren ist die Wärme zentriert und den äußeren mehr 
verteilt. 

Die Auswertung der Messwerte ergibt bei den Maximaltemperaturen (also Sensor 7), deutliche Unterschiede. Es ist ein delta von gut 17°C zu erkennen. 
:numref:`vergleich2` zeigt den Vergleich. 

.. _vergleich2:
.. figure:: ../../tex2/graphics/graphic_output/1wire_vergleich_max_test1.png
    :name: mit
    :alt: mit
    :align: center

    *Temperaturvergleich an Sensor 7*

Die am weitesten entfernten Sensoren sind die Sensoren 4 und 17. Die Auswertung der Sensoren (:numref:`vergleich3`) für die obere als auch untere Fläche, erklärt das Verhalten des 
Packs sehr gut. Innen gute Isolation gegen Wärme, daher Fall noch unter Umgebung

.. _vergleich3:
.. figure:: ../../tex2/graphics/graphic_output/1wire_vergleich_min_test1.png
    :name: mit
    :alt: mit
    :align: center

    *Temperaturvergleich an Sensoren 4 und 16*

Fazit
******

Von der Anbringung der Sensoren am äußeren Rand des Packs ist bei einem deutlichen Delta von ca. 16°C abzuraten. Auch die weitflächige Deckelerwärmung
erschwert die Detektierung des Bereichs für eine Zelle, während die erwärmte Zelle im Pack von den internen Sensoren besser lokalisiert werden kann. 


Test 2 (Vergleich Sensoren nah an Heizer in Kammern):
------------------------------------------------------

Für Test 2 werden die Sensoren nun in die Kammern zwischen den Zellen gesteckt. Es sind vier kleine Öffnungen mitten im Pack in den Zwischenräumen der
Batteriezellen. Die folgende Abbildung zeigt die Darstellung.

.. figure:: Bilder/test2_1wire1.png
    :name: mit
    :alt: mit
    :align: center

    *Aufbau von Test 2: Sensoren in den Zwischenräumen*

Auswertung
**********

Die Sensorenquadranten 5,6,7,8 und 9,10,11,12 sind besonders interessant, weil sie sich am nächsten zum Heizelement befinden. Allerdings muss die etwas stärkere
Erwärmung in Quadrant 9,10,11,12 beachtet werden, da diese etwas stärker ausgeprägt ist aufgrund der Heizelementzuleitungen, die wärmetechnisch schlecht isoliert sind.
Aufgrund dessen wird vor allem Quadrant 5,6,7,8 betrachtet wie in nachfolgender Graphik:

.. figure:: ../../tex2/graphics/graphic_output/test2_5678.png
    :name: mit
    :alt: mit
    :align: center

    *Quadrant 5,6,7,8*

Die Graphik zeigt, dass vor allem die nahen Sensoren 6 und 7 die größte Wärmemenge detektieren. Zwischen best- und schlecht detektierendem Sensoren 5 und 7
besteht ein Delta von 17°C (Sensor 5 Peak ca. 22°C/ Sensor 7 Peak ca. 39°C).

.. figure:: ../../tex2/graphics/graphic_output/test2_9101112.png
    :name: mit
    :alt: mit
    :align: center

    *Quadrant 9101112*

Die Darstellung in Quadrant 9,10,11,12 zeigt trotz stärkerer Erwärmung einen ähnlichen Messunterschied. Hier besteht zwischen den Sensoren 10 und 12 ein 
Delta von 19°C (Sensor 10 Peak ca. 51°C/ Sensor 12 Peak ca. 32°C). 

.. figure:: Bilder/test_9101112.png
    :name: mit
    :alt: mit
    :align: center

    *Wärmebild der Kammern 9,10,11 und 12*

Vergleicht man dieses Bild mit dem Bild der Wärmekamera nach Öffnung des Packs und Entnahme des Heizelements, ergibt sich ebenfalls ein Unterschied zwischen 
den Kammern 9,10,11 und 12. Die Unterschiede sind aufgrund der Abkühlung nicht so groß.
Das Bild zeigt, dass auch hier die Positionierung der Sensoren mit einem maximalen Delta von 9°C versehen ist.

Test 2.5
*********

Die Versuche werden mit einer besseren Positionierung der Sensoren wiederholt, um die Fehler des Heizelements zu kaschieren. 





Fazit
******

Die Messungen von Wärmebildkamera und Sensoren zeigen, dass die Messung vor allem von der Entfernung zum Heizelement abhängt. Die Sensoren in 
ihren "Messkammern" haben zwar gerade mal einen Abstand von 1cm, aber die Wärmeleitung über das Pack erfolgt sehr schlecht und langsam. 
Von einer Platzierung der Sensoren in den Kammern ist daher abzuraten, da eine Detektierung der Wärmemenge von 4 Zellen durch einen Sensor Schwankungen von 17
bis 19°C ausgesetzt ist. 


Test 3
*********

Der letzte Test zur Positionierung beinhaltet den Vergleich von 1-Wire Temperatursensoren auf den Kammern und in den Kammern. Aufgrund der Beschaffenheit 
der zur Verfügung stehenden Oberfläche zwischen den Zellen im Pack, lässt sich eine Platzierung der Sensoren auf den Kammern schließlich leichter
durchführen. An diesen Stellen lassen sich nämlich Platinen fertigen auf denen der Sensor angebracht ist, entweder auf die Kammer kleben oder
anbohren. 

Der abschließende Test soll zeigen, ob die deutlichen Temperaturschwankungen, die beim Einsetzen der Sensoren in den Kammern entstehen, auch bei einem Sensor 
auf den Kammern, entfallen. Es wäre dann eine folgerichtige Entscheidung, die Implementierung dort durchzuführen.

.. figure:: Bilder/test3_1wire.png
    :name: mit
    :alt: mit
    :align: center

    *Aufbau des Tests mit Sensoren auf Kammern und in Kammern*

Das obere Schaubild zeigt die Verteilung der Sensoren. Sensor 2 ist auf der Kammer rechts oben zum Heizelement geklebt und interessant für einen Vergleich mit
den Sensoren 13,14,15 und 16. Auf der anderen Seite ist Sensor 3 auf der Kammer und bietet sich für einen Vergleich mit 9,10,11 und 12 an. Die Sensoren
4,5,6,7 und 8 sind interessant für etwas entfernteren Streueffekt.

.. figure:: ../../tex2/graphics/graphic_output/test2_9101112.png
    :name: mit
    :alt: mit
    :align: left

    *Sensor oben drauf (3) im Vergleich zu nahen Sensoren in Kammern (11,12)*

.. figure:: ../../tex2/graphics/graphic_output/test2_9101112.png
    :name: mit
    :alt: mit
    :align: right

    *Sensor oben drauf (3)*