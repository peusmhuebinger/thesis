﻿.. Fortschritte documentation master file, created by
   sphinx-quickstart on Tue Sep 25 15:03:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Fortschritte
============


Fortschritt (wöchentlich)
-------------------------

.. toctree::
   :maxdepth: 2

   Protokolle/Gliederung
   Protokolle/Vorbereitung
   Protokolle/Woche1
   Protokolle/Woche2
   Protokolle/Woche3
   Protokolle/Woche4
   Protokolle/Woche5
    
   



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
