**Bachelor - Thesis docs**

Temperaturmessung und thermische Verteilung im Batterie-Pack
============================================================

Diese Dokumentation beinhaltet die wichtigsten Dokumente für die Bachelor-Thesis

---

## Schwerpunkt 1: Temperaturmessung

Die Temperaturmessung im Pack erfolgt zuverlässig, 1-2 mal pro Sekunde, über 1-Wire Temperatursensoren und über eine I2C - Datenschnittstelle

Analyse der Möglichkeiten:

1. DS2482-100
2. Microcontroller Arduino

---

## Schwerpunkt 2: thermische Analyse im Pack

Die thermische Analyse der Wärmeverteilung wird experimentell und theoretisch erörtert
Die Tests beinhalten:

1. Testen der Sensore im Pack mit unterschiedlicher Dichte/Verteilung
2. Testen der Sensoren mit Heizelement
3. Überprüfung der Ergebnisse mit Wärmebildkamera

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).