Toradex Aster Carrier Board Schematics
---------------------------

This zip file contains the Schematics for the Aster V1.1
If you have any question contact our support at: 
support@toradex.com


Change Log
----------

- Initial Release: Aster V1.0
  Release Date: 06 January 2016

- Design Revision: Aster 1.1
  PowerSupply.SchDoc : Input power connector (X2) has been replaced with new 3.5mm barrel power supply conenctor.
  PowerSupply.SchDoc : Load switch (IC15) has been added to power circuit to ensure unidirectional power flow from USB Client.
  DigitalInterface.SchDoc : Assembly option has been added to control voltage level translator OE# signal (IC11 and IC12). 
  Debugger.SchDoc : Open SDA serial debug interface is marked as not assembled.
  DisplayInterface.SchDoc : Capacitive Touch Interface connector (X3) has been added.
  VGA.SchDoc: Video DAC IC (IC10) has been used for RGB to VGA conversion.
  DigitalInterface.SchDoc, ArduinoConnector.SchDoc, UsbSerialDebug.SchDoc: Added notes (NOTE 9, 10, 11, 12).
  Release Date: 25 October 2016

- Design Revision: Aster 1.1
  PowerSupply.SchDoc: Components (IC15, C52, C53, R6, and R142) are not assembled, as Power Switch (IC15) has been found back-feeding during testing.
  Debugger.SchDoc: Colibri JTAG header (X6), JTAG pogo pins (X21) and resistor (R51) are assembled.
  Release Date: 30 November 2016

- Design Revision: Aster 1.1
  PowerSupply.SchDoc : Added note (13).
  Release Date: 27 October 2017

Important Disclaimer:
----------

Copyright � Toradex AG. All rights reserved. All data is for information purposes only and not
guaranteed for legal purposes. Information has been carefully checked and is believed to be
accurate; however, no responsibility is assumed for inaccuracies. Brand and product names are
trademarks or registered trademarks of their respective owners. Specifications are subject to
change without notice.



Trademark Acknowledgement:
----------

Brand and product names are trademarks or registered trademarks of their respective owners.
