// #include <zephyr.h> // für typedef Definitionen wie (u32_t)
#ifndef _TEMP_FUNCTIONS_H_  // falls noch nicht definiert, soll bitte im Abschnitt definiert werden
#define _TEMP_FUNCTIONS_H_   
#include <zephyr.h>

void adresses_sensor(u8_t *adre, int size);
void device_config();
int mux_select_channel(bool channel);
int get_temperature(u8_t sen_adr, float *temperature);
#endif

