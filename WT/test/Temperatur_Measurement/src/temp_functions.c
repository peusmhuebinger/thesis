#include <errno.h>
#include <zephyr.h>
#include <stdio.h>
#include <misc/printk.h>
#include <device.h>
#include <i2c.h>

#define I2C_DEVICE "I2C_1"
struct device *i2c_dev;

// 1)
void adresses_sensor(u8_t *sen_adr, int size)
{
	// Definition der Slave-Adressen befinden sich in der Main-Funktion
	u8_t sensor_first = 0x48; // Beginnende Slave-Adresse

	for (int i = 0; i < size; i++)
	{
		sen_adr[i] = sensor_first + i;
		printk("Adresse Sensor %i: 0x%x\n", i + 1, sen_adr[i]);
	}
}

// 2)
void device_config()
{
	i2c_dev = device_get_binding(I2C_DEVICE);
	if (!i2c_dev)
	{
		printk("I2C: Device driver not found.\n");
		return;
	}
}

// 3)
int mux_select_channel(bool channel)
{
	u8_t mux_adr = 0x70; //Slave-Adresse
	u8_t ctr_write = 0;  // Controlregister MUX
	u8_t ctr_read = 0;   // notwendig für die Abfrage

	if (channel == 0)
	{
		ctr_write = 0x04;
		printk("Auswahl Channel 0:\n");
	}
	else
	{
		ctr_write = 0x05;
		printk("Auswahl Channel 1:\n");
	}

	// Bei Versorgungsunterbrechung wird genau hier das Programm abgebrochen.
	
	// Kontrollregister wird geschrieben
	i2c_write(i2c_dev, &ctr_write, 1, mux_adr);
	k_busy_wait(10); // Zeit die der MUX zum einstellen benötigt

	// und gelesen/read, damit Status abgefragt wird 
	int mux_status = i2c_read(i2c_dev, &ctr_read, 1, mux_adr);
	if (mux_status != 0)
	{
		printk("Error MUX. Stat:");
		return -1;
	}

	else if (ctr_read != ctr_write)
	{
		printk("Error: Write und Read nicht gleich. Stat: ctr_read %d, ctr_write %d\n", ctr_read, ctr_write);
		return -2;
	}
	else
	{
		return 0;
	}
}

// 4)
int get_temperature(u8_t sen_adr, float *temperature)
{
	// Temperaturregister 0x00 wird geschrieben
	u8_t temp_write = 0x00;

	// Temperaturwert wird ins Register gelesen/read
	u8_t temp_read[2] = {0, 0};
	int sensor_status = i2c_write_read(i2c_dev, sen_adr, &temp_write, 1, &temp_read[0], 2);
	if (sensor_status == 0)
	{
		// [0][1], ohne <<8 wäre [leer][0|1]
		u16_t temp_hex = ((temp_read[0] << 8) | temp_read[1]) >> 5;
		*temperature = (temp_hex)*0.125;
		return 0;
	}
	else
	{
		printk("Error Temp_Sensor. Stat:");
		return -1;
	}
}
