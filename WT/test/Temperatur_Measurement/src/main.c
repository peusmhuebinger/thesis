
#include <zephyr.h>
#include <misc/printk.h>
#include <stdio.h>
#include "temp_functions.h"

#define SENSOR_COUNT 8  	// Anzahl Sensoren

void main(void)
{
	u8_t sensor_adr[SENSOR_COUNT]; // Slave-Adressen
	float temperature = 0.0;
	bool mux_channel = 0; // Channel 0 

	adresses_sensor(&sensor_adr[0], SENSOR_COUNT);
	device_config();

	while (1)
	{
		mux_channel = !mux_channel;
		int stat_mux = mux_select_channel(mux_channel); 
		if (stat_mux != 0)
		{
			printk("%i\n", stat_mux);
			return;
		}

		for (int i = 0; i < SENSOR_COUNT; i++)
		{
			//set_temp_sensor(sensor_adr[i]);
			int stat_sensor = get_temperature(sensor_adr[i], &temperature);
			if (stat_sensor == 0)
			{
				printk("CH %d\t 0x%x:\t", mux_channel, sensor_adr[i]);
				printf("T = %.3f °C\n", temperature);
			}
			else
			{
				printk("%i\n", stat_sensor);
				return;
			}
		}
		k_sleep(2000);
		printk("Neue Messung:\n");
	}
}
