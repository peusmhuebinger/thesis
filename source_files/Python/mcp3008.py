#!/usr/bin/python

import sys
import time
import subprocess
import struct
import math
import os
#import Mediator

#MEDIATOR = Mediator.Mediator('192.168.1.10', 7777)

def measure_voltage_raw(pinNumber):
    # Access the raw voltage reading
    path = "/sys/bus/iio/devices/iio:device0/in_voltage" + \
        str(pinNumber) + "_raw"
    sysRaw = subprocess.check_output(['/bin/cat', path], subprocess.STDOUT)
    return int(sysRaw.strip())


def scale(pinNumber):
    # The raw voltage reading needs to be multiplied by the scale, get the scale
    path = "/sys/bus/iio/devices/iio:device0/in_voltage" + \
        str(args.pinNumber) + "_scale"
    sysScale = subprocess.check_output(
        ['/bin/cat', path], stderr=subprocess.STDOUT)
    return float(sysScale.strip())


def getVoltage():
    vref = 3.3
    mcp3208 = 4095
    mcp3008 = 1023
    divider = float(mcp3208/vref)
    return divider


def convert_voltage_to_resistance(voltage):
    if voltage > 0:
        Rt = (10000*(3.3-voltage))/voltage
    else:
        print("GND")
        Rt = 0
    return Rt


def convert_resistance_to_Temperature(res):
    try:
        ln = math.log(res/10000)
        # print(res)
        temp = 1/(0.003354016+(0.000256985*ln)+(0.000002620131 *
                                                math.pow(ln, 2))+(0.00000006383091*math.pow(ln, 3)))-273.15
    except ValueError:
        print("Oops!  That was no valid number.  Try again...")
        temp = 0
    return temp


def read_ADC_channel(pinNumber):
    div = getVoltage()
    rawVoltage = measure_voltage_raw(pinNumber)
    measuredVoltage = float(rawVoltage / div)
    Rt = convert_voltage_to_resistance(measuredVoltage)
    temp = convert_resistance_to_Temperature(Rt)
    return temp

"""
if __name__ == '__main__':
    # remove()
    #log("Resitance 0_15 0_0")
    print("Temperatur Messung an 5 punkte & Umgebungstemperatur")
    while True:
        # for i in range (0,8):

        MEDIATOR.write_variable('temp_1', read_ADC_channel(0))
        MEDIATOR.write_variable('temp_2', read_ADC_channel(1))
        MEDIATOR.write_variable('temp_3', read_ADC_channel(2))
        MEDIATOR.write_variable('temp_4', read_ADC_channel(3))
        MEDIATOR.write_variable('temp_5', read_ADC_channel(4))
        MEDIATOR.write_variable('temp_6', read_ADC_channel(5))
        MEDIATOR.write_variable('temp_7', read_ADC_channel(6))
        MEDIATOR.write_variable('temp_8', read_ADC_channel(7))

        # log(array)
        #print("Temperature {:3.5f} ".format(array[0]))
        time.sleep(0.1)
"""
