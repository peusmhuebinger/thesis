#include <Wire.h>

uint8_t m_address = 0x18;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
	int tm1 = millis();
	begin();
	Wire.write(0xb4); 
	end();
  Serial.println(String("") +"TimeSlot: " +(millis()-tm1));
  delay(1000);
  Serial.println(String("")+ "I2C reset done");
}

void begin()
{
	Wire.beginTransmission(m_address);
	Wire.setClock(400000);
}

void end()
{
	Wire.endTransmission();
  if(Wire.endTransmission()!=0){Serial.println("Problem detected");}
}
