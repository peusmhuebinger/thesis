

#include <Wire.h>
#include <DS2482.h>

//Variablen
//Creating of an object
DS2482 DS2482(0); // 0 = 0x18, direct addressing of the DS2482 chip

uint8_t address[8];
uint8_t scratchPad[8];
byte addr_room[16][8];
byte user_address[8];
double temp = 0;
int devices_count = 0;

// Setup, starting of I2C

void setup()
{

  Serial.begin(250000);
  Serial.println("\n\nDS2482-100 Test\n\n");

  Serial.print("starting I2C: "); // Starting I2C connection
  Wire.begin(0);                  //Start I²C
  Serial.println("ok");

  scanI2C(); // Scanning I2C addresses

  Serial.print("DS2482-100 reset: ");                                          // Do the reset of the DS2482 master
  if (DS2482.reset()){Serial.println("devices detected");}                     // writes 0xB4, the 1-Wire reset impulse (reset/presence-detect cylce), requests
  Serial.println("ok");                                                        // the PPD from the status, if true return, if false process stops

  //1-Wire search algorithm

  while (DS2482.wireSearch(address))
  {                                  //search for devices and print address = true
    Serial.print("Serialnumbers: "); // true, when adress should be printed serially
    for (uint8_t i = 0; i < 8; i++)
    {                                                 // performs device reset 0xF0/ then performs one wire search algorithm and resetting of searching if
      addr_room[devices_count][i] = address[i];       // nothing found, increment the DeviceCount if something is found. when printing true, returns the
      Serial.print(addr_room[devices_count][i], HEX); // address of found 1-Wire device
    }
    devices_count++;
    Serial.println();
  }
}

void loop()
{

  //ROUTINE 1 (executed once for all 1 Wire devices)
int tmp1 = millis();
  DS2482.wireReset();
  delay(10);
  Serial.println(Wire.endTransmission());
  //reset command to 1-Wire line to enable communication
  DS2482.wireSkip(); //write 0xCC to 1-wire devices, skip ROM command, skips addresses and sends following command to all slave devices
  delay(10);
  DS2482.wireWriteByte(0x44); //Convert temperature value command, 0x44, after that wait time 100ms for conversion
  
  delay(80);

  //ROUTINE 2 (executed as long as devices_count saved)

  for (int device_now = 0; device_now < devices_count; device_now++)
  {

    for (int i = 0; i < 8; i++)
    {                                             //display of recent address room
      user_address[i] = addr_room[device_now][i]; //transceives addresses to an user_address value
    }

    DS2482.wireReset();
    delay(10);
    DS2482.wireSelect(user_address); //selecting of recent address, 0x55
    delay(10);
    DS2482.wireWriteByte(0xBE); //1-Wire command of read out of Scratchpad
    delay(10);
    for (uint8_t i = 0; i < 9; i++)
    {
      scratchPad[i] = DS2482.wireReadByte();
      delay(5);
    }


    if ((int)DS2482.crc8(scratchPad, 8) != scratchPad[8])
    {
      Serial.println("CRC error!");
    }
    else
    {
      temp = ((scratchPad[1] << 8) + scratchPad[0]) * 0.0625;
      Serial.println(String("") + "Tmp[" + device_now + "] in °C: " + temp);
    }
   }
       Serial.println("-----------------------");
    int tmp2 = millis() - tmp1;
    Serial.println(tmp2);
    Serial.println("-----------------------");
    delay(100);
}

//FUNCTIONS

//Scan I2C network

void scanI2C()
{
  for (byte address = 1; address < 127; address++)
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    byte error = Wire.endTransmission();

    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address < 16)
        Serial.print("0");
      Serial.print(address, HEX);
      Serial.println("  !");
    }
  }
}
