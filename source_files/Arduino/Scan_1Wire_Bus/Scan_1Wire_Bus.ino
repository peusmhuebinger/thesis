#include <OneWire.h>
#include <Wire.h>


OneWire dev_1;

void printAddress(uint8_t deviceAddress[8])
{
  Serial.print("{ ");
  Serial.print("0x");
  for (int i = 0; i < 8; i++)
  {
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
     }
  Serial.print(" }"); 
}

void setup()
{
  Serial.begin(9600);
    Serial.println("Checking for I2C devices...:");
  if (dev_1.checkPresence())
  {
    Serial.println("DS2482-100 present");
    
    dev_1.deviceReset();
    
    Serial.println("\tChecking for 1-Wire devices...");
    if (dev_1.wireReset())
    {
      Serial.println("\tDevices present on 1-Wire bus");
      
      uint8_t currAddress[8];
      
      Serial.println("\t\tSearching 1-Wire bus...");
      
      while (dev_1.wireSearch(currAddress))
      {
        Serial.print("\t\t\tFound device: ");
        printAddress(currAddress);
        
        Serial.println();
      }
      
      dev_1.wireResetSearch();
      
    }
    else
      Serial.println("\tNo devices on 1-Wire bus");
  }
  else
    Serial.println("No DS2482-100 present");
}

void loop()
{
  
}
