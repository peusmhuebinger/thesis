#include <Wire.h>
#include <DS2482.h>

DS2482 ds(0);  //channels ds2482-800 is 0 to 7, DS2482-100 is just set 0

byte data[8]; //holding for onewire capture
byte addr[8]; //1wire wire address and CRC
long time_temp;

void setup()
{
  Wire.begin();
  ds.reset();
        pinMode(13, OUTPUT);

  // start serial port
  Serial.begin(57600);
  time_temp=millis();}

void loop()
{
   
  //do search
 
   if ( !ds.wireSearch(addr))
     {
  Serial.print("No more addresses.\n");
        ds.wireResetSearch();

    return;
      }
     
   Serial.print("ROM =");
   for( int i = 0; i < 8; i++) {
    Serial.write(' ');
    if (addr[i] < 16) Serial.print("0");
     Serial.print(addr[i], HEX);
    }
   
   if (ds.crc8(addr, 7) != addr[7]) {
    Serial.println("CRC is not valid!");
    return;
   }
 
  //test if device code DS18b20
if (addr[0]==0x28) {

  //read temperature data.
  ds.wireReset();
  ds.wireSelect(addr);
  ds.configure(0x05); //set bus on strong pull-up after next write until next command
  ds.wireWriteByte(0x44); //convert temperature on this device
  delay(770); //wait for conversion 750ms Plus margin
  ds.wireReset();
  ds.wireSelect(addr);
  ds.wireWriteByte(0xbe);         // Read Scratchpad command

//display hex values of scratchpad
  Serial.print(" ");
  for ( int i = 0; i < 9; i++) {           // we need 9 bytes to capture CRC
    data[i] = ds.wireReadByte();

   if (data[i]<16)Serial.print("0");
   Serial.print(data[i],HEX);
   
  }
  Serial.print(" ");

//convert to decimal temperature

  int LowByte = data[0];
  int HighByte = data[1];
  int TReading = (HighByte << 8) + LowByte;
  int SignBit = TReading & 0x8000;  // test most sig bit
  if (SignBit) // negative
  {
    TReading = (TReading ^ 0xffff) + 1; // 2's comp
  }

  int Tc_100 = (double)TReading * 0.0625 * 10;

  if (SignBit) // If its negative
  {
     Tc_100=0-Tc_100;
  }
 
  //print temp for each device
   
    Serial.print(Tc_100/10);
    Serial.print(".");
    Serial.print(Tc_100%10);
    Serial.print(" ");
    Serial.print(millis() - time_temp);
    Serial.println(" ms");
}


}
