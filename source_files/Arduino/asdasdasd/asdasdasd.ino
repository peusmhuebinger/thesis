#include <OneWire.h>
#include <Wire.h>


OneWire dev_1;

uint8_t readPointer;
byte data[1];

void setup() {
 Serial.begin(9600);
 
 
 dev_1.deviceReset();                                       // 0xF0, resetting devices terminating one wire communication  
 Serial.println("1-Wire device resetted");
 
 
 uint8_t recent_address[8];
 while (dev_1.wireSearch(recent_address))                   //Search for one wire device and save in recent_address if found
      {
        Serial.print("Found device: ");
        for (int i = 0; i < 8; i++)
          {
          if (recent_address[i] < 16) Serial.print("0");    //Display address
          Serial.print(recent_address[i], HEX);
        }
        
        Serial.println();
      }
      dev_1.wireResetSearch();                              //one wire device found
      

}




void loop() {

 dev_1.deviceReset();                                       // 0xF0, resetting devices terminating one wire communication  
 Serial.println("1-Wire device reset");
 dev_1.setReadPointer(readPointer);
 Serial.println(readPointer);
 uint8_t data = dev_1.readData();
 uint8_t data2 = dev_1.wireReadByte();
 Serial.println(data, HEX);
 Serial.println(data2, HEX);
 delay(1000);
}
