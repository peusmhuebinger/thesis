#include <OneWire.h>


//1. Initialization of variables ///////////////////////////////////////////////////////
byte addr_room[16][8];
int devices_found = -1;
byte present = 0;
double temp = 0;
byte data[9];

OneWire ds(9);             //creates 1-Wire object


//2. Functions /////////////////////////////////////////////////////////////////////////

//Function 2.1: printing the hexadecimal adress of an used 1-Wire devices
void print_ROM(int device_nr){
  Serial.print("device ");
  Serial.print(device_nr);
  Serial.print(": ");
  for(int i=0;i<8;i++){
          Serial.print(addr_room[device_nr][i],HEX);
        }
 Serial.println("");       
}

//Function 2.2: printing the number of the used 1-Wire devices
void print_ROMs(){
  Serial.println(String("") + "Anzahl Sensoren: " + devices_found);
  for(int i=0;i<devices_found;i++){
          print_ROM(i);
        }
}

//Function 2.3: Calculating the temperature value in Celsius degree
void calc_temp(int device_nr){
     for (int i = 0; i < 9; i++){
    
       data[i] = ds.read();
       delay(1);
    }
    if((int)OneWire::crc8(data,8)!=data[8]){                      //CRC test, returns false when CRC proof byte is wrong
        return false;
       }
    temp = ((data[1] << 8) + data[0])*0.0625;
    Serial.print(String("") + "sensor device: " + device_nr + ", sensor Tmp: ");
    Serial.println(temp);
  }

//3. Setup settings //////////////////////////////////////////////////////////////////////
void setup() {
  // Initialization process, serial transport:
  Serial.begin(500000);
  byte addr[8];
  //device searching...
  devices_found=0;
  bool search_finished = false;
  while(!search_finished){
    bool device_found_now = ds.search(addr);      //returns true if device found, false if not
    if(device_found_now){
        for(int i=0;i<8;i++){                     //if device found,allocate adress, increment the amount of found devices
         addr_room[devices_found][i]=addr[i];
        }
         devices_found += 1; 
        }
    else{
        ds.reset_search();                       
        search_finished = true;      
        }
      }
   print_ROMs();                                  //display of all devices with HEX adresses
} 

//4. Query and calculation settings ///////////////////////////////////////////////////////
void loop() {
  Serial.println(millis());
  //Single routine
  // Routine > reset, select, command

  ds.reset();
  delay(5);
  ds.skip();                 //writes 0xCC, skips ROM command, adresses all slaves
  //Start conversion
  delay(5);
  ds.write(0x44, 1);         //writes 0x44, conversion command to all slaves
  //Conversion time
  delay(94);              

  //eight-time routine
  for(int device_now = 0;device_now < devices_found; device_now++){                 //systematically calls all found devices
      byte user_adress[8];
      present = ds.reset();     //Reset
      for(int i=0; i<8; i++){
          user_adress[i]=addr_room[device_now][i];
          delay(1);
        }
      ds.select(user_adress);   //Selection
      delay(5);
      ds.write(0xBE);            //Command                                            //command to read the scratchpad of an 1-Wire Slave with definite device number
      delay(5);
      calc_temp(device_now);
    }
  Serial.println(millis());
} 
