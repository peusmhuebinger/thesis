#include <OneWire.h>
#define DS18B20 0x28

OneWire ds(10);

void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);
}

void loop(){
byte addr[8];
int i = 0;

  if(!ds.search(addr)){
      ds.reset_search();
      return false;
     }

  //    counter+= 1;
  //    Serial.println(counter);

 Serial.print("Adresse Sensor-Slave = ");
 for(i=0;i<8;i++){
  Serial.print(addr[i],HEX);
  Serial.println(" ");
  } // address on 1wire bus

if (addr[0] == DS18B20) // check we are really using a DS18B20
{
ds.reset(); // rest 1-Wire
ds.select(addr); // select DS18B20

ds.write(0x4E); // write on scratchPad
ds.write(0x00); // User byte 0 – Unused
ds.write(0x00); // User byte 1 – Unused
ds.write(0x1F); // set up en 9 bits (0x1F)
                // set up to 10 bits (0x3F)
                // set up to 11 bits (0x5F)
                // set up to 12 bits (0x7F)

ds.reset(); // reset 1-Wire
ds.select(addr); // select DS18B20

ds.write(0x48); // copy scratchpad to EEPROM
delay(15); // wit for end of write
Serial.println("Accomplished!");

}
delay(5000);
}

//for I2C bus 
//  DS2482.reset();
//  DS2482.wireSelect(address);
//  DS2482.wireWriteByte(0x4E);
//  DS2482.wireWriteByte(0x00);
//  DS2482.wireWriteByte(0x00);
//  DS2482.wireWriteByte(0x1F);
