#include <Wire.h>
#include <DS2482.h>

  DS2482 DS2482(0);   // 0 = 0x18
  int devices_count = 0;
  uint8_t address[8];
  String SerialNumber = "";
  uint8_t scratchPad[8];
  byte addr_room[16][8];
  byte user_address[8];

  
void setup() {
  
  Serial.begin(115200);
  Serial.println("\n\nDS2482-100 Test\n\n");

  Serial.print("starting I2C: ");                                 // Starting I2C connection
  Wire.begin(0);           //Start I²C
  Serial.println("ok");
  
  scanI2C();                                                      // Scanning I2C addresses

  Serial.print("DS2482-100 reset: ");                             // Do the reset of the DS2482 master
  DS2482.reset();                                                 // writes 0xB4, the 1-Wire reset impulse (reset/presence-detect cylce), requests
                                                                  // the PPD from the status, if true return, if false process stops
  Serial.println("ok");

                                                                 //search for devices and print address = true  
                                                                 // true, when adress should be printed serially 
                                                                 // performs device reset 0xF0/ then performs one wire search algorithm and resetting of searching if
                                                                 // nothing found, increment the DeviceCount if something is found. when printing true, returns the
                                                                 // address of found 1-Wire device  
    
    
    
  

  while (DS2482.wireSearch(address)){
        Serial.print("Serialnumber: ");
        for (uint8_t i = 0; i < 8; i++){
          addr_room[devices_count][i]=address[i];
          if (address[i] < 0x10) SerialNumber += "0";
             SerialNumber += String(address[i], HEX);
             if (i < 7) SerialNumber += "-";
            }
          devices_count++;
      Serial.print(SerialNumber);
      Serial.println();
       }


}

                                

void loop() {

  //ROUTINE 1
 
  DS2482.reset();
  DS2482.wireWriteByte(0xCC);              //skip ROM command, skips addresses and sends following command to all slave 1 Wire devices
  DS2482.wireWriteByte(0x44);           //Convert temperature value command, 0x44, after that wait time 100ms for conversion      
  delay(100);

  //ROUTINE 2
  
  DS2482.reset();                                                                       //reset command
  for(int device_now = 0; device_now < devices_count; device_now++){                                                      
  for(int i = 0; i<8; i++){                                              //display of recent address room
  user_address[i] = addr_room[device_now][i];
   }  
  DS2482.wireSelect(user_address);                                                                    //selecting of recent address, 0x55 
  DS2482.wireWriteByte(0xBE);                                             //1-Wire command of read out of Scratchpad
  Serial.print("Scratchpad: ");
  for(uint8_t i = 0; i<9; i++){                                           
      scratchPad[i] = DS2482.wireReadByte();                              //display of the scratchpad results
      Serial.print(scratchPad[i],HEX);
      if (i<8){
      Serial.print("-");
      }
    }
    Serial.println(" ");
  }
  
}

void scanI2C() {
  for(byte address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    byte error = Wire.endTransmission();
 
    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.print(address, HEX);
      Serial.println("  !");
    }    
  }
}
