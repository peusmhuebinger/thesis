// Reg-Labor

clear;clc;


// Funktionsaufruf

function [betrag,phase] = complex1(c)
    re = real(c);
    im = imag(c);
    betrag = abs(c);
    phase = atan(im/re);
    phase = phase*(360/(2*%pi));
    disp('Der Betrag der komplsize(exen Zahl beträgt: ' +string(betrag)+ ' und die Phase ' +string(phase));
endfunction


