clear;clc;
// Aufgabe 4


//Parameter
theta =   1; //Massenträgheit in kgm^2
c = 1;      // Konstante der Welle in Nm/rad
d = 20;     // Konstante der Lagerung in Nms/rad

//Zeitwerte

T = 20;
dt = 0.01;
t = [0:dt:T];

//Matrizen
A = [0 1 0 0; -(c/theta) 0 (c/theta) 0; 0 0 0 1; (c/theta) 0 -(c/theta) -(d/theta) ];    //Systemmatrix
B = [0; (1/theta); 0; 0];                                                               //Eingangsmatrix
C1 = [1 0 0 0];                                                                         //Ausgangsmatrix
C2 = [0 0 1 0];
D = 0;
x0 = [0 0 0 0]';

G1 = syslin('c',A,B,C1,D,x0);
G2 = syslin('c',A,B,C2,D,x0);

//Eingangssprung
M_t = 10;
M(1:length(t)) = M_t;

//Lösung
y1 = csim(M',t,G1);
y2 = csim(M',t,G2);

//Plotten
figure(1);clf();
xgrid(9);
plot2d(t,(y1*180)/(%pi), style = 5);    //phi_1
plot2d(t,(y2*180)/(%pi), style = 3);    //phi_2
xlabel("$t\text{ in sec}$", 'fontsize', 5);
ylabel("$\text{Drehwinkel }\varphi_1\text{ bzw. }\varphi_2\text{ in Grad}$", 'fontsize', 5);
legend("$\varphi_1$", "$\varphi_2$")
f = gcf();
f.children.children(2).children.thickness = 3   //Strichdicke
f.children.children(3).children.thickness = 3   //Strichdicke
f.children.children(1).font_size = 3;           //Legendendicke
 
