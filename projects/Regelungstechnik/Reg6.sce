// Aufgabe 6

clear;clc;
xdel(winsid())
u_dach = 2;
freq1 = 5;
freq2 = 10;
delta_t = 0.01;
T = 1;
t = (0:delta_t:T);


// Funktionen

y1 = u_dach*sin(2*%pi*freq1*t);
y2 = u_dach*sin(2*%pi*freq2*t);

//Plotten der Graphen

plot2d1(t,y1);
plot2d1(t,y2);

a = gcf();
a.children(1).children(1).children.thickness = 3;
a.children(1).children(2).children.thickness = 3;
a.children.children.children(1).foreground = 3;
a.children.children.children(2).foreground = 5; 
xlabel('Zeit in Sekunden');
ylabel('Spannung in Volt');
a.children.children(1).parent.thickness = 5;
a.children.children(1).parent.font_size = 5;
a.children(1).children(1).parent.x_label.font_size = 5;
a.children(1).children(1).parent.y_label.font_size = 5;
a.children.box = "on"
xgrid(color("gray"))


xs2pdf(0,"gr.pdf")
