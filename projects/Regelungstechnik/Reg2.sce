clear; clc;

A = [2 -1 0; -1 2 -1; 0 -1 2]
t = poly(A,'s');
L = roots(t);
S = spec(A);
