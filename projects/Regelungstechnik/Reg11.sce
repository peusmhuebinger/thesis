clear; clc;
xdel(winsid());

//Aufgabe 4.1
 

//Matrizen
A = [0 1 0; 0 0 1; -110 -20.011 -10000.002];    //Systemmatrix
B = [0;0;1];
C = [110 40 20000];

D = 0;
x0 = [0 0 0]'
G = syslin('c',A,B,C,D,x0)

//Zeitvektor
T = 5000;
dt = 0.1
t = 0:dt:T-dt;
ud = 1;
u(1:length(t)) = ud;

//Plotten
xlabel("")

y = csim(u',t,G);

plot2d(t,y);

//Eigenwerte berechnen


e = roots(det(%s*eye(A)-A));
subplot(311)
plzr(G);
