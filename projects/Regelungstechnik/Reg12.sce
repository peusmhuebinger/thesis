clear;clc;
xdel(winsid());
//Aufgabe 4.1
//---------------------------------------------------------------
//Teil 1

//Zeitvektor
T = 5000;
dt = 0.1
t = 0:dt:T-dt;

//Ode - Funktion
function [xdot]=dgl2(t,x)
    xdot(1)=x(2);
    xdot(2)=x(3);
    xdot(3)=-10000.002*x(3)-20.011*x(2)-110*x(1)+1;
endfunction

//Anfangsbedingungen
x01=[0;0;0];
t0 = 0;

x=ode(x01,t0,t,dgl2);

//Ausgang
C = [110 40 20000];
y1 = C*x
//---------------------------------------------------------------
//Teil2

//Matrizen
A = [0 1 0; 0 0 1; -110 -20.011 -10000.002];    //Systemmatrix
B = [0;0;1];                                    //Eingangsmatrix
C = [110 40 20000];                             //Ausgangsmatrix

D = 0;                                          //Durchschaltmatrix
x02 = [0 0 0]'
G = syslin('c',A,B,C,D,x02)
//Sprungfunktion
ud = 1;
u(1:length(t)) = ud;

//CSIM - Verfahren
y2 = csim(u',t,G);
//---------------------------------------------------------------
//Teil3

e = roots(det(%s*eye(A)-A));                    //Berechnung der Eigenwerte

//Plotten
figure(1);clf();
subplot(311); //Plotten der ode - Lösung
xgrid();
xlabel("$\text{Zeit in Sekunden}$", 'fontsize',3);
ylabel("$y_1(t)$", 'fontsize',3)
plot2d(t,y1)
legend("$\text{Lösung mit Ode - Funktion}$")
subplot(312); //Plotten der csim - Lösung
xgrid();
xlabel("$\text{Zeit in Sekunden}$", 'fontsize',3);
ylabel("$y_2(t)$", 'fontsize',3);
plot2d(t,y2, style = 5);
legend("$\text{Lösung über den Zustandsraum}$")
subplot(313); //Plotten der Eigenwerte
xgrid();
//plzr(G); Alternativ
xlabel("$Realteil$", 'fontsize',3);
ylabel("$Imaginärteil$", 'fontsize',3);
plot2d(real(e),imag(e), style = -2, rect = [-11000, -0.15, 1000, 0.15] )
legend("$\text{Eigenwerte in der komplexen Zahlenebene}$", 3)

xs2pdf(1,"Aufgabe4.1.pdf")

