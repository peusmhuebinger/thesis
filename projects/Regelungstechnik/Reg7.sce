clear;clc;
xdel(winsid());

//Aufgabe 3.1.
// Plotten eines Frequenzspektrums mittels FFT (Fast Fourier Transformation)

//globale Variablen
Tges = 10; 
delta_f = 1/Tges; 
offset = 2; 
yd = 5;
fs = 10;  // Signalfrequenz
fa = 100; // Abtastfrequenz

//Deklaration von Frequenz- und Zeitbereichen
f = 0:1/Tges:20
t = 0:1/fa:Tges-1/fa;

//Funktionen(Sinusspannung und transformiertes Spektrum)
y = yd*sin(2*%pi*fs*t) + offset;
Spektrum = (fft(y));
//Normierung
mag = sqrt(2)*abs(Spektrum)/1000
mag(1) = mag(1)/sqrt(2)

//Plotten
//Grafikoberfläche und Rasterbildung
figure(1);clf(); xgrid();
subplot(211);
title('Frequenz: ' +string(fs) +string(' Hz')); //Frequenzausgabe im Titel
plot2d(t,y, style = 5, rect = [0,-10,5,10]);  //rect - Befehl für den Zoom
xlabel('$Zeit/sec$', 'fontsize', 3)
ylabel('$Spannung/V$', 'fontsize', 3)
subplot(212); xgrid();
plot2d2(f,mag(1:201), style = 5, rect = [0,0,20,10]); //normiertes Spektrum
xlabel('$Zeit/sec$', 'fontsize', 3)
ylabel('$Spannung/Vrms$', 'fontsize', 3)

//Ausgabe in Vektorformat
xs2jpg(1,"Aufgabe3.1.jpg")
