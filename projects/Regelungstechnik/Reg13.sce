clear;clc;
// Aufgabe 3
//Parameter

m = 2; //kg
c = 200; //N/m
d = 5;   //Ns/m
x_ruhe = 0.1; //m
v_start = 0.05 //m/s

//Zeitwerte

T = 5;
dt = 0.01;
t = [0:dt:T];

//Matrizen
A = [0 1; -(c/m) -(d/m) ];    //Systemmatrix
B = [0;(1/m)];
C = [1 0];

D = 0;
x01 = [x_ruhe v_start]'  //Bedingung Teil 1
x02 = [0 0]'             //Bedingung Teil 2
G1 = syslin('c',A,B,C,D,x01)
G2 = syslin('c',A,B,C,D,x02)


//Eingangssprung
ud1 = 0;
u1(1:length(t)) = ud1;
ud2 = 10;
u2(1:length(t)) = ud2;


y1 = csim(u1',t,G1);
y2 = csim(u2',t,G2);

//Plotten
figure(1);clf();
xgrid(9);
plot2d(t,y1, style = 5);
plot2d(t,y2, style = 3);
xlabel("$t\text{ in sec}$", 'fontsize', 5);
ylabel("$\text{Auslenkung } y_t\text{ in m}$", 'fontsize', 5);
legend("$\text{Eigenerregung}$","$\text{Kraftsprung}$");
f = gcf();
f.children.children(2).children.thickness = 3
f.children.children(3).children.thickness = 3
f.children.children(1).font_size = 3;


