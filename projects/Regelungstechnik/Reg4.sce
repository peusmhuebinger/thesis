clear;clc;

fa = 8000;
l = 0.1;
A=0.5
fstart=2000 
df=500
fende=9000

t = 0:1/fa:l-1/fa;

for fs = fstart:df:fende
    y = A*sin(2*%pi*fs*t);
    printf("Die angegebene Frequenz liegt bei: %6.1f Hz\n" ,fs)
    sound(y,fa,16);
end
