clear;clc;

T = 1;
fre = 9;
fa = 1000;
f = 0:1:999
offset = 2;
yd = 3;
t = 0:0.001:0.999;
y = yd*sin(2*%pi*fre*t) + offset;
Spektrum = (fft(y));

//Normierung
mag = sqrt(2)*abs(Spektrum)/1000
mag(1) = mag(1)/sqrt(2)

plot2d(t,y);
plot2d(f,mag(1:1000), style = 5);
