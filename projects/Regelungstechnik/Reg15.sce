clear;clc;
xdel();

//Aufgabe 15                                             11.01.17
//Levenberg-Marquardt-Methode für Parameteridentifikation

//Daten einlesen aus der Datei A52.dat
daten = fscanfMat('A52.dat');
t = daten(:,1);
drehzahl = daten(:,2);

//Anfangswerte anhand der verrauschten Messwerte identifizieren
T1 = 2;
T2 = 1000;
K = 1000;

//Modellfunktion
function y = modell(par)
    T1 = par(1);
    T2 = par(2);
    K = par(3);
    y = K*(1-(T1*exp((-t)/T1)-T2*exp((-t)/T2))/(T1 - T2))
endfunction

//Fehlerfunktion
function e = err_PT2(par,m)
    e = drehzahl - modell(par); 
endfunction
//Levenberg-Marquardt-Methode zur Modell
[p,err] =lsqrsolve([T1,T2,K],err_PT2,length(t));
Modell = modell(p);

//mittler effektiver Fehler
mef = norm(err)/length(t);

//Plotten
figure(1);clf();
xgrid(9);
plot2d(t,Modell, style = 5); //Modell nach PT2-Glied
plot2d(t,drehzahl,-3); //verrauschte Drehzahl
//Graphen bearbeiten
title(sprintf('Mittlerer effektiver Fehler: %5.3g',mef));
xlabel('$\text{Zeit in sec}$', 'fontsize', 3);
ylabel('$\text{Drehzahl in }min^\text{-1}$', 'fontsize',3)
fkt = gca();
fkt.children.children(2).thickness = 3;

//xs2pdf(1,"Aufgabe5.21.pdf")



