Altium Designer Pick and Place Locations
D:\PRJ\hardware\BatMan\FlexPCB_TermoSensors\Gesign\Project Outputs for FlexPCB_TermoSensors\Pick Place for FlexPCB_TermoSensors.txt

========================================================================================================================
File Design Information:

Date:       11.03.19
Time:       16:48
Revision:   Not in VersionControl
Variant:    No variations
Units used: mil

Designator Comment Layer    Footprint Center-X(mil) Center-Y(mil) Rotation Description     
U1         PCT2075 TopLayer SO-8-flex 6259.606      4137.599      0        ""              
U2         PCT2075 TopLayer SO-8-flex 8543.071      4137.599      0        ""              
U3         PCT2075 TopLayer SO-8-flex 10826.535     4137.599      0        ""              
U4         PCT2075 TopLayer SO-8-flex 13110.000     4137.599      0        ""              
U5         PCT2075 TopLayer SO-8-flex 17047.008     4137.599      0        ""              
U6         PCT2075 TopLayer SO-8-flex 19330.472     4137.599      0        ""              
U7         PCT2075 TopLayer SO-8-flex 21613.937     4137.599      0        ""              
U8         PCT2075 TopLayer SO-8-flex 23897.402     4137.599      0        ""              
U9         PCT2075 TopLayer SO-8-flex 27834.410     4137.599      0        ""              
U10        PCT2075 TopLayer SO-8-flex 30117.874     4137.599      0        ""              
U11        PCT2075 TopLayer SO-8-flex 32401.339     4137.599      0        ""              
U12        PCT2075 TopLayer SO-8-flex 34684.803     4137.599      0        ""              
U13        PCT2075 TopLayer SO-8-flex 38621.811     4137.598      0        ""              
U14        PCT2075 TopLayer SO-8-flex 40905.276     4137.598      0        ""              
U15        PCT2075 TopLayer SO-8-flex 43188.740     4137.598      0        ""              
U16        PCT2075 TopLayer SO-8-flex 45472.205     4137.598      0        ""              
X1         ""      TopLayer FLEX4     2816.449      4134.000      0        "Header, 4-Pin" 
