EESchema Schematic File Version 2
LIBS:sensor_halter-rescue
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:sensor_halter-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x02 J2
U 1 1 5BD56B5E
P 6250 3550
F 0 "J2" H 6250 3650 50  0000 C CNN
F 1 "Conn_out" H 6250 3350 50  0000 C CNN
F 2 "Connectors_Molex:Molex_PicoBlade_53047-0210_02x1.25mm_Straight" H 6250 3550 50  0001 C CNN
F 3 "" H 6250 3550 50  0001 C CNN
	1    6250 3550
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J1
U 1 1 5BD56BA1
P 4450 3550
F 0 "J1" H 4450 3650 50  0000 C CNN
F 1 "Conn_in" H 4450 3350 50  0000 C CNN
F 2 "Connectors_Molex:Molex_PicoBlade_53047-0210_02x1.25mm_Straight" H 4450 3550 50  0001 C CNN
F 3 "" H 4450 3550 50  0001 C CNN
	1    4450 3550
	-1   0    0    -1  
$EndComp
$Comp
L AP1117-33-RESCUE-sensor_halter U1
U 1 1 5BD56F56
P 5400 3550
F 0 "U1" H 5750 3775 50  0000 C CNN
F 1 "MAX31820PAR" H 5900 3775 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 5900 3850 50  0001 C CNN
F 3 "" H 5500 3300 50  0001 C CNN
	1    5400 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 3650 5000 3650
Wire Wire Line
	5000 3650 5000 3850
Wire Wire Line
	5000 3850 5700 3850
Wire Wire Line
	5700 3850 5700 3550
Connection ~ 5400 3850
Wire Wire Line
	5100 3550 4650 3550
Wire Wire Line
	5000 3550 5000 3400
Wire Wire Line
	5000 3400 6050 3400
Wire Wire Line
	6050 3400 6050 3550
Connection ~ 5000 3550
Wire Wire Line
	5700 3550 5900 3550
Wire Wire Line
	5900 3550 5900 3650
Wire Wire Line
	5900 3650 6050 3650
$EndSCHEMATC
